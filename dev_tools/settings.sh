# Copyright 2020 The Quark Developers
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#!/usr/bin/env bash
#
# local settings
#

# name of project (necessary, please replace with actual project name)
PROJECT=quark
VERSION=0.1
AUTHORS='The QC developers of DLR SC'

# optionally add settings below that overwrite global ones from 
#     modules/development_tools/environment/settings.sh or
#     modules/development_tools/checks/settings.sh
