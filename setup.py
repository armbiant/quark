""" setup for project quark """

from setuptools import setup, find_packages


def get_requirements():
    """ get the requirements """
    requirements = []
    with open('requirements.txt') as f:
        requirements.append(f.readlines())
    with open('requirements_pip.txt') as f:
        requirements.append(f.readlines())
    return requirements


setup(name = 'quark',
      version = '0.1',
      url = 'https://gitlab.com/quantum-computing-software/quark',
      author = 'The QC developers of DLR SC',
      author_email = 'elisabeth.lobe@dlr.de',
      python_requires = '>=3.8',
      install_requires = get_requirements(),
      description = 'QUARK (QUantum Application Reformulation Kernel) is a software package to support the mapping of '
                    'combinatorial optimization problems to quantum computing interfaces via QUBO and Ising problems.',
      packages = find_packages(include = ["quark"]),
      zip_safe = False)
