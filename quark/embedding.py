# Copyright 2022 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for Embedding """

from functools import cached_property
from itertools import combinations, product
import networkx as nx

from quark import HardwareAdjacency

ERROR_MAPPINGS  = "Either provide mapping from variables to nodes or to edges"
ERROR_DIFFERENT = "The var_edges_map cannot contain different variables than var_nodes_map"
ERROR_CONNECTED = "Cannot check connectivity without mapping from variables to edges"
ERROR_VALID     = "Cannot judge validity without mapping from couplings to edges"


class Embedding:
    """
    An embedding is a mapping from variables and couplings of the original problem to the nodes and edges of a hardware
    graph, defining the embedding subgraphs.
    """

    def __init__(self, var_nodes_map=None, coupling_edges_map=None, var_edges_map=None, name=None, index=None):
        """
        initialize Embedding object,
        need at least either var_nodes_map or var_edges_map

        :param (dict or list[list] or None) var_nodes_map: mapping of variables to nodes in the hardware graph,
                                                           if not given, it is extracted from var_edges_map
        :param (dict or None) coupling_edges_map: mapping of couplings to edges in the hardware graph
        :param (dict or list[list] or None) var_edges_map: mapping of variables to edges in the hardware graph
        :param (str or None) name: identifying name to differ between several embeddings
        :param (int or None) index: identifying index to differ between several embeddings with the same name
        """
        if not var_nodes_map and not var_edges_map:
            raise ValueError(ERROR_MAPPINGS)
        if var_edges_map and not var_nodes_map:
            var_nodes_map = {var : sorted(set(node for edge in edges for node in edge))
                             for var, edges in var_edges_map.items()}

        self.name = name
        self.index = index
        self.var_nodes_map = var_nodes_map if isinstance(var_nodes_map, dict) else dict(enumerate(var_nodes_map))
        self.var_edges_map = var_edges_map if isinstance(var_edges_map, dict) else dict(enumerate(var_edges_map or []))
        self.coupling_edges_map = coupling_edges_map or {}
        self.coupling_edges_map = {tuple(sorted(coupling)): edges
                                   for coupling, edges in self.coupling_edges_map.items()}
        self.check_consistency()

    def __eq__(self, other):
        if isinstance(other, Embedding):
            return self.var_nodes_map == other.var_nodes_map \
                   and self.var_edges_map == other.var_edges_map \
                   and self.coupling_edges_map == other.coupling_edges_map \
                   and self.name == other.name \
                   and self.index == other.index
        return False

    def check_consistency(self):
        """ check if the data is consistent """
        if self.var_edges_map and set(self.var_edges_map.keys()) != set(self.var_nodes_map.keys()):
            raise ValueError(ERROR_DIFFERENT)

    @cached_property
    def nodes(self):
        """ all nodes used in the embedding """
        return sorted(set(node for nodes in self.var_nodes_map.values() for node in nodes))

    @cached_property
    def edges(self):
        """ all edges used in the embedding """
        edges = set()
        if self.var_edges_map:
            edges.update(edge for edges in self.var_edges_map.values() for edge in edges)
        if self.coupling_edges_map:
            edges.update(edge for edges in self.coupling_edges_map.values() for edge in edges)
        return sorted(edges)

    @cached_property
    def max_map_size(self):
        """ the maximum size (number of nodes) of the embedding subgraph of some variable """
        return 0 if not self.var_nodes_map else max(len(nodes) for nodes in self.var_nodes_map.values())

    @classmethod
    def get_from_hwa(cls, var_nodes_map, hwa, name=None, index=None):
        """
        initialize embedding with information calculated from the hardware graph

        :param (dict or list[list]) var_nodes_map: mapping of variables to nodes in the hardware graph
        :param (list[tuples]) hwa: hardware adjacency defining the hardware graph to which the embedding should fit
        :param (str or None) name: identifying name to differ between several embeddings,
                                   by default the name of the HWA
        :param (int or None) index: identifying index to differ between several embeddings with the same name
        :return: the embedding compatible to the hwa
        """
        var_nodes_map = var_nodes_map if isinstance(var_nodes_map, dict) else dict(enumerate(var_nodes_map))
        var_edges_map = get_var_edges_map(var_nodes_map, hwa)
        coupling_edges_map = get_coupling_edges_map(var_nodes_map, hwa)
        name = name or (hwa.name if isinstance(hwa, HardwareAdjacency) else None)
        return cls(var_nodes_map, coupling_edges_map, var_edges_map, name=name, index=index)

    def is_valid(self, couplings=None):
        """
        check if the embedding is valid in general
        and, if given, also checks if there is at least one edge in the hardware graph for each coupling

        :param (set[tuples] or list[tuples] or None) couplings: list of couplings of the original problem
        :return: True if the embedding is valid
        """
        return self.are_all_node_sets_disjoint() and self.are_all_subgraphs_connected() and self.are_all_edges_valid() \
               and (couplings is None or self.are_all_couplings_mapped(couplings))

    def are_all_node_sets_disjoint(self):
        """
        check if the node sets do have nodes in common

        :return: True if all node sets are disjoint
        """
        for set1, set2 in combinations(self.var_nodes_map.values(), 2):
            if not set(set1).isdisjoint(set2):
                return False
        return True

    def are_all_subgraphs_connected(self):
        """
        check if all embeddings form a connected subgraph

        :return: True if all embedding subgraphs are connected
        """
        if not self.var_edges_map:
            raise ValueError(ERROR_CONNECTED)
        for variable, edges in self.var_edges_map.items():
            # empty embeddings are not valid
            if not self.var_nodes_map[variable]:
                return False
            # in turn, embeddings consisting of a single node, do not have edges, but are validly 'connected'
            if len(self.var_nodes_map[variable]) > 1:
                subgraph = nx.Graph()
                subgraph.add_edges_from(edges)
                if not subgraph or not nx.algorithms.is_connected(subgraph):
                    return False
        return True

    def are_all_edges_valid(self):
        """
        check if all edges for a coupling indeed connect the nodes sets of the corresponding variables

        :return: True if all edges connect the node sets
        """
        for coupling, edges in self.coupling_edges_map.items():
            for edge in edges:
                if not (edge[0] in self.var_nodes_map[coupling[0]] and edge[1] in self.var_nodes_map[coupling[1]]) and \
                        not (edge[1] in self.var_nodes_map[coupling[0]] and edge[0] in self.var_nodes_map[coupling[1]]):
                    return False
        return True

    def are_all_couplings_mapped(self, couplings):
        """
        check if there is at least one edge in the hardware graph for each coupling

        :param (set[tuples] or list[tuples]) couplings: set of couplings of the original problem
        :return: True if all couplings do have a counterpart in the hardware
        """
        if not self.coupling_edges_map:
            raise ValueError(ERROR_VALID)
        return all(self.coupling_edges_map.get(tuple(sorted(coupling)), []) for coupling in couplings)


def get_var_edges_map(var_nodes_map, hwa):
    """
    get the edges in the embedding subgraph for each variable

    :param (dict or list[list]) var_nodes_map: mapping of variables to nodes in the hardware graph
    :param (list[tuples]) hwa: hardware adjacency defining the hardware graph
    :return: the mapping of variables to edges in the hardware graph
    """
    return {var: get_edges_among(nodes, hwa) for var, nodes in var_nodes_map.items()}

def get_edges_among(nodes, hwa):
    """
    get the edges in the embedding subgraph defined by the given nodes

    :param (set or list) nodes: set of nodes in the hardware graph
    :param (list[tuples]) hwa: hardware adjacency defining the hardware graph
    :return: the corresponding edges in the hardware graph
    """
    return [tuple(sorted((n1, n2))) for n1, n2 in combinations(nodes, 2) if (n1, n2) in hwa or (n2, n1) in hwa]

def get_coupling_edges_map(var_nodes_map, hwa):
    """
    get the edges connecting the embedding subgraphs of each variable pair

    :param (dict or list[list]) var_nodes_map: mapping of variables to nodes in the hardware graph
    :param (list[tuples]) hwa: hardware adjacency defining the hardware graph
    :return: the mapping of couplings to edges in the hardware graph
    """
    return {tuple(sorted((n1, n2))): get_edges_between(var_nodes_map[n1], var_nodes_map[n2], hwa) for
            n1, n2 in combinations(var_nodes_map.keys(), 2)}

def get_edges_between(nodes1, nodes2, hwa):
    """
    get the edges connecting the embedding subgraphs defined by the two node sets

    :param (set or list) nodes1: first set of nodes in the hardware graph
    :param (set or list) nodes2: second set of nodes in the hardware graph
    :param (list[tuples]) hwa: hardware adjacency defining the hardware graph
    :return: the edges in the hardware graph connecting the corresponding embedding subgraphs
    """
    check = hwa.are_neighbored if isinstance(hwa, HardwareAdjacency) \
        else lambda n1, n2: (n1, n2) in hwa or (n2, n1) in hwa
    return sorted(tuple(sorted((n1, n2))) for n1, n2 in product(nodes1, nodes2) if check(n1, n2))
