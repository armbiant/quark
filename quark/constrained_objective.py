# Copyright 2020 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for ConstraintObjective """

from abc import abstractmethod

from quark import PolyBinary, ObjectiveTerms


OBJECTIVE      = "objective"
REDUCTION      = "reduction"
OBJECTIVE_POLY = "objective_poly"
CONSTRAINTS    = "constraints"

ERROR_INIT = "Currently only binary objective functions are supported for the ConstrainedObjective"
ERROR_IMPL = "Provide '{0}' on initialization or implement '_get_{0}' in inheriting 'ConstrainedObjective' class"


class ConstrainedObjective(dict):
    """
    An objective function combined with usually multiple constraints, restricting the set of feasible solutions,
    together provide an optimization problem.

    This class either serves as a container for a pre-calculated objective polynomial with constraints
    or as a base class for the implementation of methods providing the objective polynomial and the constraints.

    (It is analogue to ObjectiveTerms but with storing constraints.)
    """

    def __init__(self, objective_poly=None, constraints=None, instance=None):
        """
        initialize a ConstrainedObjective object,
        either provide objective_poly and constraints to store them or
        provide instance for implemented subclass to construct the objective_poly and constraints from the instance

        :param (PolyBinary or None) objective_poly: polynomial representing the objective function
        :param (dict or None) constraints: dictionary with names to constraints
        :param instance: instance object storing all data to construct the constrained objective from
        """
        # allow for empty objective polynomial
        self.objective_poly = objective_poly if objective_poly is not None else self._get_objective_poly(instance)
        if not isinstance(self.objective_poly, PolyBinary):
            raise ValueError(ERROR_INIT)
        # also allow the constraints to be set explicitly to []
        super().__init__(constraints if constraints is not None else self._get_constraints(instance))

    def __eq__(self, other):
        if isinstance(other, ConstrainedObjective):
            if super().__eq__(other):
                return self.objective_poly == other.objective_poly
        return False

    def __ne__(self, other):
        # needs to be implemented otherwise would just use dict comparison
        return not self.__eq__(other)

    def get_broken_constraints(self, var_assignment):
        """
        get the names of the constraints that the variable assignment does not fulfill

        :param (dict or list) var_assignment: mapping from variables to values
        :return: the names of the constraints that are not fulfilled
        """
        return [name for name, constraint in self.items() if not constraint.check_validity(var_assignment)]

    def check_validity(self, var_assignment):
        """
        check if the variable assignment fulfills the constraints

        :param (dict or list) var_assignment: mapping from variables to values
        :return: True if all constraints are fulfilled
        """
        return not self.get_broken_constraints(var_assignment)

    def get_objective_terms(self, objective_name=OBJECTIVE, combine_prefixes=(REDUCTION,)):
        """
        get an ObjectiveTerms object with the objective as one term
        and automatically generated penalty terms for each constraint

        :param (str) objective_name: name of the term that corresponds to the objective
        :param (tuple or str) combine_prefixes: prefixes in the names of the constraints whose corresponding
                                                objective terms shall be combined to a single objective term,
                                                by default all terms starting with "reduction" are combined
        :return: the ObjectiveTerms object
        """
        objective_terms = {}
        for name, constraint in self.items():
            objective_terms.update(constraint.get_penalty_terms(name))
        constraint_terms_names = list(objective_terms.keys())
        if not self.objective_poly == 0:
            # only in case the objective polynomial is not empty we do want to add it as an additional term
            objective_terms.update({objective_name: self.objective_poly})

        obj = ObjectiveTerms(objective_terms=objective_terms, constraint_terms_names=constraint_terms_names)
        if combine_prefixes:
            combine_prefixes = combine_prefixes if isinstance(combine_prefixes, tuple) else (combine_prefixes,)
            return obj.combine_prefixes(*combine_prefixes)
        return obj

    def get_all_variables(self):
        """
        get all variables from all polynomials

        :return: the sorted list of all appearing variables
        """
        variables = set(self.objective_poly.variables)
        for constraint in self.values():
            variables.update(constraint.polynomial.variables)
        return sorted(variables)

    @staticmethod
    @abstractmethod
    def _get_objective_poly(instance):
        """
        get the objective polynomial from the instance data,
        needs to be implemented in subclasses for automatic generation

        :param instance: instance object storing all data to construct the objective polynomial from
        :return: the polynomial representing the objective function
        """
        raise NotImplementedError(ERROR_IMPL.format(OBJECTIVE_POLY))

    @staticmethod
    @abstractmethod
    def _get_constraints(instance):
        """
        get the constraints from the instance data,
        needs to be implemented in subclasses for automatic generation

        :param instance: instance object storing all data to construct the constraints from
        :return: the dictionary with names to constraints
        """
        raise NotImplementedError(ERROR_IMPL.format(CONSTRAINTS))
