# Copyright 2020 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for ObjectiveTerms """

from abc import abstractmethod

from quark import PolyBinary, PolyIsing, Objective


CONSTRAINT_TERMS_NAMES = "constraint_terms_names"
OBJECTIVE_TERMS = "objective_terms"

ERROR_IMPL = "Provide '{0}' on initialization or implement '_get_{0}' in inheriting 'ObjectiveTerms' class"
ERROR_NAMES = "All constraint terms names should also be a key of the objective terms"
ERROR_MISMATCH = "There is a mismatch between given and stored objective_terms names"
ERROR_TYPE = "Cannot add PolyIsing and PolyBinary, make sure your objective terms are all of the same " \
             "type by using the methods 'all_to_xxx'"


class ObjectiveTerms(dict):
    """
    An objective term is a quadratic polynomial that belongs to a specific part of an objective function of a QUBO or
    an Ising problem. To distinguish the terms, they are named. Several objective terms are weighted and summed up to
    finally form the full objective polynomial.

    This class either serves as a container for pre-calculated objective terms
    or as the base class for the implementation of methods providing the objective terms.

    (It is an analogue to the ConstrainedObjective but with storing objective terms.)
    """

    def __init__(self, objective_terms=None, constraint_terms_names=None, instance=None):
        """
        initialize ObjectiveTerms object,
        either provide objective_terms to store them or
        provide instance for implemented subclass to construct the objective_poly and constraints from the instance

        :param (dict or None) objective_terms: mapping from names to objective terms
        :param (list[str] or None) constraint_terms_names: names of objective terms that correspond to constraints
        :param instance: instance object storing all data to construct the objective terms from
        """
        # the objective terms should not be empty
        super().__init__(objective_terms or self._get_objective_terms(instance))

        # also allow the constraint_terms_names to be set explicitly to []
        if constraint_terms_names is not None:
            self.constraint_terms_names = constraint_terms_names
        else:
            self.constraint_terms_names = self._get_constraint_terms_names()

        self.check_consistency()

    def check_consistency(self):
        """ check if the data is consistent """
        if not set(self.constraint_terms_names) <= set(self.keys()):
            raise ValueError(ERROR_NAMES)

    def __eq__(self, other):
        if isinstance(other, ObjectiveTerms):
            if super().__eq__(other):
                return set(self.constraint_terms_names) == set(other.constraint_terms_names)
        return False

    def __ne__(self, other):
        # needs to be implemented otherwise would just use dict comparison
        return not self.__eq__(other)

    def _check_names(self, names):
        if set(names) != set(self.keys()):
            raise ValueError(ERROR_MISMATCH)

    def evaluate_terms(self, var_assignment):
        """
        get the values of all objective terms evaluated with the given variable assignment

        :param (dict) var_assignment: mapping from variables to values
        :return: the dictionary of objective terms names to values
        """
        return {name: objective_term.evaluate(var_assignment) for name, objective_term in self.items()}

    def check_validity(self, var_assignment):
        """
        check if the given variable assignment fulfills the condition
        that the value of all objective terms corresponding to constraints should be 0

        :param (dict) var_assignment: mapping from variables to values
        :return: True if all objective terms corresponding to constraints evaluate to 0
        """
        values = self.evaluate_terms(var_assignment)
        return all(values[name] == 0 for name in self.constraint_terms_names)

    def get_objective_name(self, terms_weights):
        """
        get a string representation of the terms weights,
        the keys need to be the same as the objective term names
        the ordering in the string is defined by the ordering in the terms weights dictionary
        (e.g. 'myConstraint1.0_mySecondConstraint1.5')

        :param (dict) terms_weights: mapping of objective terms names to values
        :return: the name consisting of the objective terms names with the corresponding values
        """
        self._check_names(terms_weights.keys())
        return get_string(terms_weights)

    def get_weighted_sum_polynomial(self, terms_weights):
        """
        get the weighted sum of the objective terms as a polynomial

        :param (dict) terms_weights: mapping of objective terms names to values
        :return: the weighted sum of the objective terms
        """
        self._check_names(terms_weights)
        try:
            return sum(terms_weights[name] * self[name] for name in terms_weights)
        except ValueError as ve:
            raise ValueError(ERROR_TYPE) from ve

    def get_objective(self, terms_weights, name=None):
        """
        get the objective with a polynomial objective function given by the weighted sum of the objective terms

        :param (dict) terms_weights: mapping of objective terms names to values
        :param (str or None) name: name of the constructed objective, by default generated from terms weights
        :return: the Objective with the weighted sum of the objective terms as objective function
        """
        poly = self.get_weighted_sum_polynomial(terms_weights)
        name = name or self.get_objective_name(terms_weights)
        return Objective(polynomial=poly, name=name)

    def combine_prefixes(self, *prefixes):
        """
        sum up those objective terms whose names share the given prefixes

        :param (str) prefixes: the prefixes of the objective terms names that shall be combined
        :return: the new ObjectiveTerms object with fewer terms
        """
        new_terms = dict(self)
        new_names = set(self.constraint_terms_names)
        for prefix in prefixes:
            combine = {name: term for name, term in self.items() if name.startswith(prefix)}
            if combine:
                for name in combine.keys():
                    del new_terms[name]
                    if name in new_names:
                        new_names.remove(name)
                        new_names.add(prefix)
                new_terms[prefix] = sum(combine.values())
        return self.__class__(objective_terms=new_terms, constraint_terms_names=sorted(new_names))

    def all_to_binary(self):
        """
        convert all objective terms to PolyBinary

        :return: the new ObjectiveTerms with all terms as binary polynomials
        """
        new_terms = {name: PolyBinary.from_unknown_poly(poly) for name, poly in self.items()}
        return self.__class__(objective_terms=new_terms, constraint_terms_names=self.constraint_terms_names)

    def all_to_ising(self, inverted=False):
        """
        convert all objective terms to PolyIsing

        :param (bool) inverted: if True, convert to inverted Ising
        :return: the new ObjectiveTerms with all terms as Ising polynomials
        """
        new_terms = {name: PolyIsing.from_unknown_poly(poly, inverted=inverted) for name, poly in self.items()}
        return self.__class__(objective_terms=new_terms, constraint_terms_names=self.constraint_terms_names)

    def get_default_terms_weights(self, penalty_scale=1):
        """
        get a default weighting of the objective terms to create the objective,
        can be overwritten in subclass implementations to provide more meaningful weights

        :param (numbers.Real) penalty_scale: the scaling factor which is applied to the weights of terms corresponding
                                             to constraints
        :return: the default terms weights
        """
        return {name: penalty_scale if name in self.constraint_terms_names else 1 for name in self.keys()}

    @staticmethod
    @abstractmethod
    def _get_constraint_terms_names():
        """
        get the names of the objective terms whose value must be zero to have the corresponding constraints fulfilled,
        which need to be a subset of all objective terms names,
        needs to be implemented in subclasses for automatic generation

        :return: the list of objective terms names representing the constraints
        """
        raise NotImplementedError(ERROR_IMPL.format(CONSTRAINT_TERMS_NAMES))

    @staticmethod
    @abstractmethod
    def _get_objective_terms(instance):
        """
        get the objective terms from the instance data,
        needs to be implemented in subclasses for automatic generation

        :param instance: instance object storing all data to construct the objective terms from
        :return: the dictionary with names to objective terms (Polynomials)
        """
        raise NotImplementedError(ERROR_IMPL.format(OBJECTIVE_TERMS))


def get_string(terms_weights):
    """
    get a string concatenating the given dictionary of terms weights,
    the ordering in the string is defined by the ordering in the terms weights dictionary
    (e.g. myConstraint1.0_mySecondConstraint1.5)

    :param (dict) terms_weights: mapping of objective terms names to values
    :return: the string consisting of the objective terms names with the corresponding values
    """
    pw_list = []
    for name in terms_weights.keys():
        pw_list.append(name + f'{terms_weights[name]:e}')
    return '_'.join(pw_list)
