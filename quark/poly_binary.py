# Copyright 2020 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for PolyBinary """

from warnings import warn
from itertools import combinations

from quark import Polynomial
import quark.poly_ising
from quark.utils import variables


ADD       = "add"
MULTIPLY  = "multiply"
REDUCTION = "reduction"
X         = "x"

ERROR_ISING  = "Cannot {} PolyBinary and PolyIsing"
ERROR_DEGREE = "Not applicable for polynomials with degree larger than 2"
ERROR_UPPER  = "Upper bound needs to be larger than or equal 0"
WARNING_FLAT = "Flat variables are replaced by tuple variables"

STUPID = "stupid_but_supposedly_fastest"
BETTER = "probably_better"
BEST   = "supposedly_best_but_slowest"


class PolyBinary(Polynomial):
    """
    A binary polynomial is a special polynomial, where the variables can only take 0 or 1.
    Since 0^2 = 0 and 1^2 = 1, we can simplify the Polynomial by removing all exponents,
    e.g. (0, 0, 1, 1, 1): 1 -> (0, 1): 1,
         (2, 2, 2, 2): 2    -> (2,): 2.

    We have the following conventions:
        *  PolyBinary == PolyBinary <=> poly == poly,
        *  PolyBinary == Polynomial <=> poly == poly,
        *  PolyBinary +/* Polynomial = PolyBinary,
        *  Polynomial +/* PolyBinary = Polynomial + Warning,
        *  PolyIsing +/* PolyBinary = Error,
        *  copy/remove_zero_coefficients/replace_variables/get_rounded(PolyBinary) = PolyBinary,
        *  evaluate(PolyBinary, {var : Number}) = PolyBinary/Number,
        *  evaluate(PolyBinary, {var : Polynomial}) = Polynomial,
        *  evaluate(PolyBinary, {var : PolyBinary}) = Error,
        *  PolyBinary == PolyIsing = False.
    """

    def _format_variable_tuple(self, var_tuple):
        """
        transform the tuple of variables into the standard format, also checks the type of the variables,
        extends 'Polynomial._format_variable_tuple',
        since we have x * x = x  for all x in {0, 1}, all powers can be removed
        """
        var_tuple = super()._format_variable_tuple(var_tuple)
        var_tuple = tuple(sorted(set(var_tuple)))
        return var_tuple

    def __eq__(self, other):
        """
        check equality of polynomials, where PolyIsing is never equal to a PolyBinary,
        extends 'Polynomial.__eq__'
        """
        if isinstance(other, quark.poly_ising.PolyIsing):
            return False
        return super().__eq__(other)

    @staticmethod
    def _check_different(poly2, func_name):
        """ check if the type of the second Polynomial is feasible for an operation """
        if isinstance(poly2, quark.poly_ising.PolyIsing):
            raise ValueError(ERROR_ISING.format(func_name))

    def __add__(self, poly2):
        """
        add another polynomial or a scalar,
        extends 'Polynomial.__add__' since PolyIsing cannot be added to PolyBinary
        """
        self._check_different(poly2, ADD)
        return super().__add__(poly2)

    def __mul__(self, poly2):
        """
        multiply with another polynomial or a scalar,
        extends 'Polynomial.__mul__' since PolyIsing cannot be multiplied with PolyBinary
        """
        self._check_different(poly2, MULTIPLY)
        return super().__mul__(poly2)

    @classmethod
    def _is_valid_var_assignment(cls, var_assignment):
        """
        check whether the given input can be assigned to the variable,
        that means it is a Polynomial, PolyBinary, SCIP Expr or valid coefficient,
        extends 'Polynomial._is_valid_var_assignment' to not allow for PolyIsing
        """
        return not isinstance(var_assignment, PolyBinary) and super()._is_valid_var_assignment(var_assignment)

    def is_non_negative(self):
        """
        check if the polynomial has only non-negative coefficients
        and thus always evaluates to non-negative values
        """
        return all(coeff >= 0 for coeff in self.values())

    def get_matrix_representation(self):
        """
        get the polynomial in the standard matrix representation

        :return: the quadratic matrix of the standard representation
        """
        linear, quadratic = super().get_matrix_representation()
        for i, value in enumerate(linear):
            quadratic[i, i] = value
        return quadratic

    # additional methods for binary polynomials

    @classmethod
    def from_unknown_poly(cls, poly):
        """
        convert a polynomial of unknown type into a PolyBinary

        :param poly: the polynomial of unknown type
        :return: the corresponding binary polynomial
        """
        return poly.to_binary() if isinstance(poly, quark.poly_ising.PolyIsing) else cls(poly)

    @staticmethod
    def preprocess_rule(var_sigmas, coeff, unambitious=False):
        """
        very simple preprocessing function:
        if the weight of the variable is positive and extends the influence of incoming negative couplings,
        the variable can be assigned to 0,
        if the weight is negative and extends the influence of incoming positive couplings, can be assigned to 1

        :param (tuple) var_sigmas: sigma values (combined incoming strengths) of a variable of the polynomial
        :param (Real) coeff: coefficient on the linear term of the variable
        :param (bool) unambitious: if True, only those variables will be set where their assignment is unambitious,
                                   that means, in all optimal solutions, they have the same value,
                                   if False, preprocess the variable to the straightforward value whether it might also
                                   get another value in an optimal solution, the objective values remains the same
        :return: the solution value or None if the variable cannot be preprocessed
        """
        if unambitious and coeff == -var_sigmas[1]:
            return None
        if coeff >= -var_sigmas[1]:
            return 0
        if coeff <= -var_sigmas[0]:
            return 1
        return None

    def to_ising(self, inverted=False):
        """
        convert the binary polynomial to the Ising format

        :param (bool) inverted: if False, use conversion 0 -> -1, 1 -> 1 with x = 0.5s + 0.5,
                                if True, use conversion  0 -> 1, 1 -> -1 with x = -0.5s + 0.5
        :return: the corresponding PolyIsing
        """
        converted = self.affine_transform(-0.5, 0.5) if inverted else self.affine_transform(0.5, 0.5)
        return quark.poly_ising.PolyIsing(converted, inverted)

    def reduce(self, max_degree=1, reduction_variable_prefix=(REDUCTION,), var_pair_choice=STUPID):
        """
        reduce the polynomial by substituting every variable product with a new variable until it is quadratic
        and get the corresponding penalty terms enforcing the reduction

        :param (tuple or str) reduction_variable_prefix: prefix for the additional reduction variables that are created
        :param (int) max_degree: the maximal degree of the final polynomial
        :param (str) var_pair_choice: the option how to choose the next variable pair
        :return: the reduced polynomial, the dictionary of penalty terms enforcing the reductions
        """
        if self.degree <= max_degree:
            return self, {}
        poly = self
        if self.is_flat():
            # in the following we need to introduce new variables which are of type tuple
            # as we cannot mix up different types of variables we need to rename the flat variables
            warn(WARNING_FLAT)
            poly = self.replace_variables(lambda i: (X, i))
        if not isinstance(reduction_variable_prefix, tuple):
            reduction_variable_prefix = (reduction_variable_prefix,)

        poly_dict, penalty_terms = _reduce_poly_dict(poly, max_degree, reduction_variable_prefix, var_pair_choice)
        reduced_poly = PolyBinary(poly_dict)
        return reduced_poly, penalty_terms


def _reduce_poly_dict(poly_dict, max_degree, reduction_variable_prefix, var_pair_choice):
    penalty_terms = {}
    current_degree = max(map(len, poly_dict.keys()))

    while current_degree > max_degree:
        var_pair_choice = STUPID if current_degree <= 2 else var_pair_choice
        var1, var2 = _get_best_variable_pair(poly_dict, current_degree, option=var_pair_choice)
        reduction_var = reduction_variable_prefix + var1 + var2
        poly_dict = _replace_variable_pair(poly_dict, var1, var2, reduction_var)
        reduction_name = variables.to_string(reduction_var)
        penalty_terms[reduction_name] = get_reduction_penalty_polynomial(var1, var2, reduction_var)
        current_degree = max(map(len, poly_dict.keys()))
    return poly_dict, penalty_terms

def _get_best_variable_pair(poly_dict, degree, option=BEST):
    if option == STUPID:
        max_monomial = max(poly_dict.keys(), key=len)
        var1, var2 = max_monomial[0:2]
        return var1, var2

    max_var_pairs = sum((list(combinations(var_tuple, 2))
                         for var_tuple in poly_dict.keys() if len(var_tuple) == degree), start=[])
    if option == BETTER:
        return max(sorted(set(max_var_pairs)), key=max_var_pairs.count)

    if option == BEST:
        all_var_pairs = sum((list(combinations(var_tuple, 2)) for var_tuple in poly_dict.keys()), start=[])
        return max(sorted(set(max_var_pairs)), key=all_var_pairs.count)

    raise ValueError(f"Unknown option '{option}'")

def _replace_variable_pair(poly_dict, var1, var2, reduction_var):
    reduced_poly_dict = {}
    for var_tuple, coeff in poly_dict.items():
        if var1 in var_tuple and var2 in var_tuple:
            new_var_tuple = tuple(var for var in var_tuple if var not in [var1, var2]) + (reduction_var,)
            reduced_poly_dict[new_var_tuple] = coeff
        else:
            reduced_poly_dict[var_tuple] = coeff
    return reduced_poly_dict


def get_reduction_penalty_polynomial(factor_x, factor_y, product_xy):
    """
    get the quadratic polynomial that enforces a variable to be equal to the product of two variables
    (the variables need to be of the same type, thus either tuple, int or str)

    :param (tuple or int or str) factor_x: first binary variable
    :param (tuple or int or str) factor_y: second binary variable
    :param (tuple or int or str) product_xy: new binary variable that substitutes x*y
    :return: the PolyBinary that is zero if x * y = xy and positive otherwise
    """
    return PolyBinary({(product_xy,): 3,
                       (factor_x, factor_y): 1,
                       (factor_x, product_xy): -2,
                       (factor_y, product_xy): -2})

def get_binary_representation_polynomial(upper_bound, variable_prefix=(X,)):
    """
    get the linear polynomial representing an integer variable bounded between 0 and the upper bound
    using the binary representation with binary variables,
    the polynomial will be empty if upper_bound=0

    :param (Real) upper_bound: value of the upper bound
    :param (tuple or str) variable_prefix: prefix for the binary variables that are created
    :return: the linear polynomial representing the integer variable
    """
    if upper_bound < 0:
        raise ValueError(ERROR_UPPER)
    if not isinstance(variable_prefix, tuple):
        variable_prefix = (variable_prefix,)

    num_variables = int(upper_bound).bit_length()
    remaining = upper_bound - 2 ** (num_variables - 1) + 1
    return PolyBinary({((*variable_prefix, i),): _get_bin_coefficient(i, num_variables, remaining)
                       for i in range(num_variables)})

def _get_bin_coefficient(index, number_of_variables, remaining):
    """ get the coefficient of the binary representation polynomial for a certain index """
    return 2 ** index if index < number_of_variables - 1 else remaining
