# Copyright 2020 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for ConstraintBinary """

from numbers import Real
from warnings import warn

from quark.poly_binary import PolyBinary, get_binary_representation_polynomial


CONSTRAINT = "constraint"
SLACK      = "slack"
REDUCTION  = "reduction"
X          = "x"

ERROR_CONSTANT      = "Constant must be a number"
ERROR_BOTH          = "Both bounds need to be given, unbounded constraints cannot be handled"
ERROR_LOWER         = "Lower bound must be less than or equal to upper bound"
ERROR_UNFULFILLABLE = "Unfulfillable constraint '{}'"
ERROR_INT           = "Inequality constraint must have integer coefficients and boundaries"
ERROR_NEGATIVE      = "Polynomial with non-negative coefficients shall sum up to negative value"
ERROR_BINARY        = "At the moment can only handle PolyBinary"
ERROR_LINEAR        = "Method only makes sense for linear polynomials"

WARNING_USELESS = "Useless constraint '{}'"
WARNING_FLAT    = "Flat variables are replaced by tuple variables"


class ConstraintBinary:
    """
    A constraint restricts the values of a function, here a polynomial.
    We define an inequality constraint with lower_bound <= polynomial <= upper_bound,
    where we have an equality constraint if lower_bound == upper_bound.
    """

    def __init__(self, polynomial, lower_bound, upper_bound):
        """
        initialize ConstraintBinary object,
        both bounds need to be given, because unbounded constraints cannot be handled

        :param (PolyBinary) polynomial: binary polynomial which is bounded
        :param (Real) lower_bound: value of the lower bound
        :param (Real) upper_bound: value of the upper bound
         """
        self.lower_bound = lower_bound
        self.upper_bound = upper_bound
        self.polynomial = polynomial

        self.check_consistency()

    def check_consistency(self):
        """ check if the data is consistent and defines a meaningful constraint """
        if self.lower_bound is None or self.upper_bound is None:
            raise ValueError(ERROR_BOTH)
        if self.lower_bound > self.upper_bound:
            raise ValueError(ERROR_LOWER)
        if not isinstance(self.polynomial, PolyBinary):
            raise ValueError(ERROR_BINARY)
        if self.polynomial.is_constant():
            if not self.lower_bound <= self.polynomial.offset <= self.upper_bound:
                raise ValueError(ERROR_UNFULFILLABLE.format(self.to_string()))
            warn(WARNING_USELESS.format(self.to_string()))
        if self.is_equality_constraint():
            normalized_constraint = self.get_normalized()
            if normalized_constraint.polynomial.is_non_negative() and normalized_constraint.polynomial.offset > 0:
                # a negative value on the other side of a non-negative polynomial is a contradiction
                raise ValueError(ERROR_NEGATIVE)

    def __add__(self, const):
        """
        shift the constraint by adding the constant to the polynomial, the lower_bound and the upper_bound

        :param (Real) const: constant value to be added
        :return: the shifted constraint
        """
        if not isinstance(const, Real):
            raise ValueError(ERROR_CONSTANT)
        return ConstraintBinary(self.polynomial + const, self.lower_bound + const, self.upper_bound + const)

    def __sub__(self, const):
        """
        shift the constraint by subtracting the constant from the polynomial, the lower_bound and the upper_bound

        :param (Real) const: constant value to be subtracted
        :return: the shifted constraint
        """
        return self.__add__(-const)

    def __mul__(self, const):
        """
        scale the constraint by multiplying the constant to the polynomial, the lower_bound and the upper_bound

        :param (Real) const: constant value to be multiplied
        :return: the scaled constraint
        """
        if not isinstance(const, Real):
            raise ValueError(ERROR_CONSTANT)
        return ConstraintBinary(self.polynomial * const, self.lower_bound * const, self.upper_bound * const)

    def __eq__(self, other):
        if isinstance(other, ConstraintBinary):
            diff = self.lower_bound - other.lower_bound
            if self.polynomial == other.polynomial + diff and self.upper_bound == other.upper_bound + diff:
                return True
            scale = self.lower_bound / other.lower_bound
            return self.polynomial == other.polynomial * scale and self.upper_bound == other.upper_bound * scale
        return False

    def __repr__(self):
        return str((repr(self.polynomial), self.lower_bound, self.upper_bound))

    def is_equality_constraint(self):
        """ check if the constraint is an equality constraint """
        return self.lower_bound == self.upper_bound

    def is_normal(self):
        """ check if the constraint is in normalized format """
        return self.lower_bound == 0

    def is_integer(self, precision=1e-6):
        """
        check if the lower bound, the upper bound and the coefficients of the polynomial are all integer

        :param (Real) precision: precision value until which values are considered to be integer
        :return: True if all values are integer
        """
        return self.polynomial.has_int_coefficients(precision) \
               and abs(self.lower_bound - int(self.lower_bound)) < precision \
               and abs(self.upper_bound - int(self.upper_bound)) < precision

    def to_string(self):
        """
        get nicely formatted representation of the constraint

        :return: the nice string representation of the constraint
        """
        if self.is_equality_constraint():
            return f"{self.polynomial.to_string()} == {self.upper_bound}"
        return f"{self.lower_bound} <= {self.polynomial.to_string()} <= {self.upper_bound}"

    def get_normalized(self):
        """
        overwrite the constraint with the normalized format where lower_bound=0
        by subtracting the lower bound
        """
        if self.is_normal():
            return self
        return ConstraintBinary(self.polynomial - self.lower_bound, 0, self.upper_bound - self.lower_bound)

    def reduce_to_linear(self):
        """
        get a new constraint with just a linear polynomial by substituting products of variables

        :return the reduced constraint, the dictionary of penalty terms for the reduction
        """
        if self.polynomial.degree >= 2:
            linear_polynomial, reduction_penalty_terms = self.polynomial.reduce()
            linear_constraint = ConstraintBinary(linear_polynomial, self.lower_bound, self.upper_bound)
            return linear_constraint, reduction_penalty_terms
        return self, {}

    def get_penalty_terms(self, name=CONSTRAINT, precision=1e-6):
        """
        get a quadratic polynomial term which represents the constraint,
        that is, it evaluates to 0 if the constraint is fulfilled and to a positive value otherwise,
        (thus penalizes invalid variable assignments)

        if necessary, slack variables are added and/or a quadratic polynomial is reduced to a linear one,
        the latter causes additional penalty term(s), such that we get, e.g.,
            {name: penalty term directly derived from constraint,
            'reduction_x_1_y_1': penalty term for reduction of product x_1*y_1,
            ...}

        :param (str) name: name of the penalty term directly derived from constraint
        :param (Real) precision: precision value for checking if the polynomial is integer, which is needed in case of
                                 an inequality constraint, because those can only be handled if they are integer
        :return: the dictionary with names to terms
        """
        if self.polynomial.is_constant():
            # in this case the constraint is not actually a constraint
            # invalid constructions should be caught by initialization
            assert self.lower_bound <= self.polynomial.offset <= self.upper_bound
            return {}
        if not (self.is_integer(precision) or self.upper_bound == self.lower_bound):
            # we can only handle integer inequality constraints or arbitrary equality constraints
            raise ValueError(ERROR_INT)

        normalized_constraint = self.get_normalized()
        if self.is_equality_constraint() and normalized_constraint.polynomial.is_non_negative():
            # in this case the polynomial can be used itself as a penalty term as it is >=0 for all assignments
            # invalid constructions should be caught by initialization
            assert normalized_constraint.polynomial.offset <= 0
            return {name: normalized_constraint.polynomial}

        linear_constraint, penalty_terms = normalized_constraint.reduce_to_linear()
        penalty_terms.update(linear_constraint.get_linear_inequality_penalty_term(name))
        return penalty_terms

    def get_linear_inequality_penalty_term(self, name):
        """
        get the penalty term representing the inequality constraint bounding a linear polynomial

        :param (str) name: name of the penalty term
        :return: the dictionary with one entry {name: penalty term for constraint}
        """
        normalized_constraint = self.get_normalized()
        poly = get_linear_inequality_penalty_polynomial(normalized_constraint.polynomial,
                                                        normalized_constraint.upper_bound,
                                                        (name + '_' + SLACK,))
        return {name: poly}

    def check_validity(self, var_assignment):
        """
        check if a given variable assignment fulfills the constraint

        :param (dict or list) var_assignment: mapping from variables to values
        :return: True if the constraint is fulfilled
        """
        poly_value = self.polynomial.evaluate(var_assignment)
        return self.lower_bound <= poly_value <= self.upper_bound


def get_linear_inequality_penalty_polynomial(polynomial, upper_bound, slack_variable_prefix=(SLACK,)):
    """
    get the quadratic polynomial term representing the constraint of the form '0 <= (linear) polynomial <= upper_bound'

    this is done by transforming the inequality to an equality with 'polynomial == s' for '0 <= s <= upper_bound'
    where s is an integer slack variable, which gets replaced by a binary representation,
    then the penalty term is formed with '(polynomial - s)**2'

    :param (PolyBinary) polynomial: linear polynomial of the constraint
    :param (Real) upper_bound: value of the upper bound
    :param (tuple or str) slack_variable_prefix: prefix for the additional slack variables that are created
    :return: the quadratic polynomial representing the constraint
    """
    if not polynomial.is_linear():
        raise ValueError(ERROR_LINEAR)

    slack_polynomial = get_binary_representation_polynomial(upper_bound, slack_variable_prefix)
    return (polynomial - slack_polynomial) ** 2
