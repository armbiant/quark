# Copyright 2020 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for example implementations of base classes for testing """

from quark.testing.example_instance import ExampleInstance
from quark.testing.example_objective_terms import ExampleObjectiveTerms
from quark.testing.example_constrained_objective import ExampleConstrainedObjective
from quark.testing.example_objective import ExampleObjective
from quark.testing.example_config import ExampleConfig
