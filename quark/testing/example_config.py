# Copyright 2020 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for ExampleConfig """

from quark.utils.config import Config


class ExampleConfig(Config):
    """ example implementation of a configuration for testing """

    def __init__(self,
                 float_value=None,
                 int_value=None,
                 str_value=None,
                 list_of_floats=None,
                 dict_value=None,
                 set_defaults=False):
        """
        initialize ExampleConfig object

        :param float_value: option with float
        :param int_value: option with int
        :param str_value: option with str
        :param list_of_floats: option with list of floats
        :param dict_value: option with dict
        :param set_defaults: if True, set options which are not given explicitly to the default values
        """
        super().__init__(set_defaults,
                         float_value=float_value,
                         int_value=int_value,
                         list_of_floats=list_of_floats,
                         str_value=str_value,
                         dict_value=dict_value)

    @staticmethod
    def get_options_dict():
        """ get the dictionary of all possible options and additional information such as their default values """
        return dict(float_value   =dict(type=float, default=20.0,            help='Just a float for testing'),
                    int_value     =dict(type=int,   default=10,              help='Just an int for testing'),
                    str_value     =dict(type=str,   default="some_string",   help='Just a str for testing'),
                    list_of_floats=dict(type=list,  default=None,            help='Just a list of floats for testing'),
                    dict_value    =dict(type=dict,  default=dict(default=0), help='Just a dict for testing'))
