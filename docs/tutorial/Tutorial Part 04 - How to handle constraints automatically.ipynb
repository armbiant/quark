{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial Part 04 - How to handle constraints automatically"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from quark import PolyBinary, ConstraintBinary, ConstrainedObjective\n",
    "from quark.io import Instance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We reuse the instance definition from Tutorial 02"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = 'x'\n",
    "COLORED_EDGES = 'colored_edges'\n",
    "ONE_COLOR_PER_NODE = 'one_color_per_node'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MaxKColorableSubgraphInstance(Instance):\n",
    "    \n",
    "    def __init__(self, edges, num_colors):\n",
    "        \"\"\" initialize max-k-colorable subgraph problem\n",
    "        Args:\n",
    "            edges(list): list of tuples representing edges of the graph\n",
    "            num_colors(int): number of colors\n",
    "        \"\"\"\n",
    "        self.edges = edges\n",
    "        self.nodes = sorted(set(node for edge in self.edges for node in edge))\n",
    "        self.num_colors = num_colors\n",
    "        super().__init__()\n",
    "        \n",
    "    def check_consistency(self):\n",
    "        for edges in self.edges:\n",
    "            if len(edges) != 2:\n",
    "                raise ValueError(\"Instance is not consistent. Edges need to have two nodes.\")\n",
    "        if self.num_colors < 1:\n",
    "            raise ValueError(\"Instance is not consistent. We need at least one color.\")\n",
    "        return True"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "num_colors = 3\n",
    "edges = [('a', 'b'), ('a', 'c'), ('b', 'd'), ('c', 'd')]\n",
    "\n",
    "instance = MaxKColorableSubgraphInstance(edges=edges, num_colors=num_colors)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What we will learn\n",
    "\n",
    "In the previous tutorials you saw how to handle constraints by explicitly adding additional terms (penalty terms) to to QUBO. quark provides a way to do this automatically. This tutorial will cover\n",
    "\n",
    " 1. how to deal with constraints and \n",
    " 2. how to construct the QUBO automatically from a problem with constraints."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Constraints"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Constraints restrict certain variable assignments. Those are usually given in the form \n",
    "\n",
    "$$ l \\leq p(x) \\leq u$$\n",
    "\n",
    "where $p$ is a polynomial and $l$ and $u$ are the upper and lower bound, respectively. If we have $l = u$ we have an equality constraint. Those constraints can easily be transformed into penalty terms which can be added to the QUBO objective function. This is automized in the ``ConstraintBinary`` class"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Considering node $n=0$ we have the constraint \n",
    "\n",
    "$$ \\sum_{c=0}^{k-1} x_{0c} = 1$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "which can be implemented with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'+1 x_a_0 +1 x_a_1 +1 x_a_2 == 1'"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "poly = PolyBinary({((X, 'a', color),): 1 for color in range(instance.num_colors)})\n",
    "constraint = ConstraintBinary(poly, 1, 1)\n",
    "constraint.to_string()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can automatically get the penalty term with "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'one_color_per_node_a': {(): 1, (('x', 'a', 0),): -1, (('x', 'a', 1),): -1, (('x', 'a', 2),): -1, (('x', 'a', 0), ('x', 'a', 1)): 2, (('x', 'a', 0), ('x', 'a', 2)): 2, (('x', 'a', 1), ('x', 'a', 2)): 2}}"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "term = constraint.get_penalty_terms(ONE_COLOR_PER_NODE + '_a')\n",
    "term"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also 'real' inequalities are supported and up to quadratic polynomials, however in this case the transformation steps are much more complicated and result in even more complicated penalty terms (and probably more than just a single one per constraint). \n",
    "\n",
    "Just as an example, let's restrict the square of the above polynomial with \n",
    "\n",
    "$$ 1 \\leq \\left(\\sum_{c=0}^{k-1} x_{\\text{a}c}\\right)^2 \\leq 2$$\n",
    "\n",
    "we get "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'1 <= +1 x_a_0 +1 x_a_1 +1 x_a_2 +2 x_a_0 x_a_1 +2 x_a_0 x_a_2 +2 x_a_1 x_a_2 <= 2'"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "constraint = ConstraintBinary(poly * poly, 1, 2)\n",
    "constraint.to_string()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'reduction_x_a_0_x_a_1': {(('reduction', 'x', 'a', 0, 'x', 'a', 1),): 3, (('reduction', 'x', 'a', 0, 'x', 'a', 1), ('x', 'a', 0)): -2, (('reduction', 'x', 'a', 0, 'x', 'a', 1), ('x', 'a', 1)): -2, (('x', 'a', 0), ('x', 'a', 1)): 1},\n",
       " 'reduction_x_a_0_x_a_2': {(('reduction', 'x', 'a', 0, 'x', 'a', 2),): 3, (('reduction', 'x', 'a', 0, 'x', 'a', 2), ('x', 'a', 0)): -2, (('reduction', 'x', 'a', 0, 'x', 'a', 2), ('x', 'a', 2)): -2, (('x', 'a', 0), ('x', 'a', 2)): 1},\n",
       " 'reduction_x_a_1_x_a_2': {(('reduction', 'x', 'a', 1, 'x', 'a', 2),): 3, (('reduction', 'x', 'a', 1, 'x', 'a', 2), ('x', 'a', 1)): -2, (('reduction', 'x', 'a', 1, 'x', 'a', 2), ('x', 'a', 2)): -2, (('x', 'a', 1), ('x', 'a', 2)): 1},\n",
       " 'constraint': {(): 1, (('constraint_slack', 0),): 3, (('reduction', 'x', 'a', 0, 'x', 'a', 1),): 0, (('reduction', 'x', 'a', 0, 'x', 'a', 2),): 0, (('reduction', 'x', 'a', 1, 'x', 'a', 2),): 0, (('x', 'a', 0),): -1, (('x', 'a', 1),): -1, (('x', 'a', 2),): -1, (('constraint_slack', 0), ('reduction', 'x', 'a', 0, 'x', 'a', 1)): -4, (('constraint_slack', 0), ('reduction', 'x', 'a', 0, 'x', 'a', 2)): -4, (('constraint_slack', 0), ('reduction', 'x', 'a', 1, 'x', 'a', 2)): -4, (('constraint_slack', 0), ('x', 'a', 0)): -2, (('constraint_slack', 0), ('x', 'a', 1)): -2, (('constraint_slack', 0), ('x', 'a', 2)): -2, (('reduction', 'x', 'a', 0, 'x', 'a', 1), ('reduction', 'x', 'a', 0, 'x', 'a', 2)): 8, (('reduction', 'x', 'a', 0, 'x', 'a', 1), ('reduction', 'x', 'a', 1, 'x', 'a', 2)): 8, (('reduction', 'x', 'a', 0, 'x', 'a', 1), ('x', 'a', 0)): 4, (('reduction', 'x', 'a', 0, 'x', 'a', 1), ('x', 'a', 1)): 4, (('reduction', 'x', 'a', 0, 'x', 'a', 1), ('x', 'a', 2)): 4, (('reduction', 'x', 'a', 0, 'x', 'a', 2), ('reduction', 'x', 'a', 1, 'x', 'a', 2)): 8, (('reduction', 'x', 'a', 0, 'x', 'a', 2), ('x', 'a', 0)): 4, (('reduction', 'x', 'a', 0, 'x', 'a', 2), ('x', 'a', 1)): 4, (('reduction', 'x', 'a', 0, 'x', 'a', 2), ('x', 'a', 2)): 4, (('reduction', 'x', 'a', 1, 'x', 'a', 2), ('x', 'a', 0)): 4, (('reduction', 'x', 'a', 1, 'x', 'a', 2), ('x', 'a', 1)): 4, (('reduction', 'x', 'a', 1, 'x', 'a', 2), ('x', 'a', 2)): 4, (('x', 'a', 0), ('x', 'a', 1)): 2, (('x', 'a', 0), ('x', 'a', 2)): 2, (('x', 'a', 1), ('x', 'a', 2)): 2}}"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "terms = constraint.get_penalty_terms()\n",
    "terms"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'+1 +3 constraint_slack_0 +0 reduction_x_a_0_x_a_1 +0 reduction_x_a_0_x_a_2 +0 reduction_x_a_1_x_a_2 -1 x_a_0 -1 x_a_1 -1 x_a_2 -4 constraint_slack_0 reduction_x_a_0_x_a_1 -4 constraint_slack_0 reduction_x_a_0_x_a_2 -4 constraint_slack_0 reduction_x_a_1_x_a_2 -2 constraint_slack_0 x_a_0 -2 constraint_slack_0 x_a_1 -2 constraint_slack_0 x_a_2 +8 reduction_x_a_0_x_a_1 reduction_x_a_0_x_a_2 +8 reduction_x_a_0_x_a_1 reduction_x_a_1_x_a_2 +4 reduction_x_a_0_x_a_1 x_a_0 +4 reduction_x_a_0_x_a_1 x_a_1 +4 reduction_x_a_0_x_a_1 x_a_2 +8 reduction_x_a_0_x_a_2 reduction_x_a_1_x_a_2 +4 reduction_x_a_0_x_a_2 x_a_0 +4 reduction_x_a_0_x_a_2 x_a_1 +4 reduction_x_a_0_x_a_2 x_a_2 +4 reduction_x_a_1_x_a_2 x_a_0 +4 reduction_x_a_1_x_a_2 x_a_1 +4 reduction_x_a_1_x_a_2 x_a_2 +2 x_a_0 x_a_1 +2 x_a_0 x_a_2 +2 x_a_1 x_a_2'"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "terms['constraint'].to_string()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Automatic QUBO construction from problem with constraints\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The max-k-colorable subgraph problem has an equality constraint. \n",
    "In contrast to tutorial 01, we will not explicity construct the qubo with terms corresponding to constraints, but just use the cost function\n",
    "\n",
    "$$C = \\sum_{(n,m)\\in E} \\sum_{c=0}^{k-1} x_{nc} x_{mc}$$\n",
    "\n",
    "as well as the constraints\n",
    "\n",
    "$$ \\sum_{c=0}^{k-1} x_{nc} = 1 \\qquad \\forall n \\in N$$\n",
    " \n",
    "to construct an object handling the cost function as well as the constraints explicitly: ``ContrainedObjective``. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MaxKColorableSubgraphConstrainedObjective(ConstrainedObjective):\n",
    "    \"\"\"\n",
    "    class containing the objective and the constraints for a max-k-colorable subgraph problem\n",
    "    \"\"\"\n",
    "\n",
    "    @staticmethod\n",
    "    def _get_objective_poly(instance):\n",
    "        \"\"\" construct the objective polynomial \"\"\"\n",
    "        # counting the number of colored edges:\n",
    "        # sum_[c in Colors] sum_[(n,m) in Edges] (1 * x_n_c * x_m_c)\n",
    "        return PolyBinary({((X, node_1, color), (X, node_2, color)): 1 for node_1, node_2 in instance.edges\n",
    "                                                                       for color in range(instance.num_colors)})\n",
    "\n",
    "    @staticmethod\n",
    "    def _get_constraints(instance):\n",
    "        \"\"\" construct the constraints \"\"\"\n",
    "        constraints = {}\n",
    "        \n",
    "        # every node should get exactly one color:\n",
    "        # for all n in Nodes: sum_[c in Colors] x_n_c == 1\n",
    "        for node in instance.nodes:\n",
    "            poly = PolyBinary({((X, node, color),): 1 for color in range(instance.num_colors)})\n",
    "            constraints[ONE_COLOR_PER_NODE + f'_{node}'] = ConstraintBinary(poly, 1, 1)\n",
    "        return constraints\n",
    "\n",
    "    @staticmethod\n",
    "    def get_original_problem_solution(raw_solution):\n",
    "        \"\"\" extract the actual solution from the variable assignment \"\"\"\n",
    "        return {node : color for (_, node, color), value in raw_solution.items() if isclose(value, 1)}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's create such an object"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "constrained_objective = MaxKColorableSubgraphConstrainedObjective(instance=instance)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This object now holds the cost function as a polynomial"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'+1 x_a_0 x_b_0 +1 x_a_0 x_c_0 +1 x_a_1 x_b_1 +1 x_a_1 x_c_1 +1 x_a_2 x_b_2 +1 x_a_2 x_c_2 +1 x_b_0 x_d_0 +1 x_b_1 x_d_1 +1 x_b_2 x_d_2 +1 x_c_0 x_d_0 +1 x_c_1 x_d_1 +1 x_c_2 x_d_2'"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "constrained_objective.objective_poly.to_string()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "as well as a dictionary of constaints (as we defined it in the definition ``MaxKColorableSubgraphConstrainedObjective``, one for each node in the original graph). For our particular graph, this means\n",
    "\n",
    "$$ \\sum_{c=0}^{2} x_{\\text{a}c} = 1 $$\n",
    "$$ \\sum_{c=0}^{2} x_{\\text{b}c} = 1 $$\n",
    "$$ \\sum_{c=0}^{2} x_{\\text{c}c} = 1 $$\n",
    "$$ \\sum_{c=0}^{2} x_{\\text{d}c} = 1 $$\n",
    "\n",
    "which are stored as (note the ``ConstrainedObjective`` base class inherits from ``dict``):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'one_color_per_node_a': <quark.constraint_binary.ConstraintBinary at 0x1a84a870a00>,\n",
       " 'one_color_per_node_b': <quark.constraint_binary.ConstraintBinary at 0x1a84a873cd0>,\n",
       " 'one_color_per_node_c': <quark.constraint_binary.ConstraintBinary at 0x1a84a873ac0>,\n",
       " 'one_color_per_node_d': <quark.constraint_binary.ConstraintBinary at 0x1a84a8731f0>}"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "#NBVAL_IGNORE_OUTPUT\n",
    "constrained_objective"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From this, we can proceed and construct objective terms as it was done in tutorial 01 (with the only difference that we have 4 penalty terms here)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'one_color_per_node_a': {(): 1, (('x', 'a', 0),): -1, (('x', 'a', 1),): -1, (('x', 'a', 2),): -1, (('x', 'a', 0), ('x', 'a', 1)): 2, (('x', 'a', 0), ('x', 'a', 2)): 2, (('x', 'a', 1), ('x', 'a', 2)): 2},\n",
       " 'one_color_per_node_b': {(): 1, (('x', 'b', 0),): -1, (('x', 'b', 1),): -1, (('x', 'b', 2),): -1, (('x', 'b', 0), ('x', 'b', 1)): 2, (('x', 'b', 0), ('x', 'b', 2)): 2, (('x', 'b', 1), ('x', 'b', 2)): 2},\n",
       " 'one_color_per_node_c': {(): 1, (('x', 'c', 0),): -1, (('x', 'c', 1),): -1, (('x', 'c', 2),): -1, (('x', 'c', 0), ('x', 'c', 1)): 2, (('x', 'c', 0), ('x', 'c', 2)): 2, (('x', 'c', 1), ('x', 'c', 2)): 2},\n",
       " 'one_color_per_node_d': {(): 1, (('x', 'd', 0),): -1, (('x', 'd', 1),): -1, (('x', 'd', 2),): -1, (('x', 'd', 0), ('x', 'd', 1)): 2, (('x', 'd', 0), ('x', 'd', 2)): 2, (('x', 'd', 1), ('x', 'd', 2)): 2},\n",
       " 'colored_edges': {(('x', 'a', 0), ('x', 'b', 0)): 1, (('x', 'a', 0), ('x', 'c', 0)): 1, (('x', 'a', 1), ('x', 'b', 1)): 1, (('x', 'a', 1), ('x', 'c', 1)): 1, (('x', 'a', 2), ('x', 'b', 2)): 1, (('x', 'a', 2), ('x', 'c', 2)): 1, (('x', 'b', 0), ('x', 'd', 0)): 1, (('x', 'b', 1), ('x', 'd', 1)): 1, (('x', 'b', 2), ('x', 'd', 2)): 1, (('x', 'c', 0), ('x', 'd', 0)): 1, (('x', 'c', 1), ('x', 'd', 1)): 1, (('x', 'c', 2), ('x', 'd', 2)): 1}}"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "objective_terms = constrained_objective.get_objective_terms(objective_name=COLORED_EDGES)\n",
    "objective_terms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And continue to construct a QUBO like this\n",
    "\n",
    "$$Q(x) = \\sum_{(n, m)\\in E} \\sum_{c=0}^{k-1} x_{nc} x_{mc} + \\sum_{n \\in N} \\lambda_n \\left(\\sum_{c=0}^{k-1} x_{nc} - 1 \\right)^2$$\n",
    "\n",
    "For simplicity we set all factors equal $\\lambda_n = \\lambda_{\\text{one}} \\forall n \\in N$ so that we have the same case as in tutorial 01"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "terms_weights = {COLORED_EDGES: 1,\n",
    "                 'one_color_per_node_a': 10,\n",
    "                 'one_color_per_node_b': 10,\n",
    "                 'one_color_per_node_c': 10,\n",
    "                 'one_color_per_node_d': 10}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{(): 40, (('x', 'a', 0),): -10, (('x', 'a', 1),): -10, (('x', 'a', 2),): -10, (('x', 'b', 0),): -10, (('x', 'b', 1),): -10, (('x', 'b', 2),): -10, (('x', 'c', 0),): -10, (('x', 'c', 1),): -10, (('x', 'c', 2),): -10, (('x', 'd', 0),): -10, (('x', 'd', 1),): -10, (('x', 'd', 2),): -10, (('x', 'a', 0), ('x', 'a', 1)): 20, (('x', 'a', 0), ('x', 'a', 2)): 20, (('x', 'a', 0), ('x', 'b', 0)): 1, (('x', 'a', 0), ('x', 'c', 0)): 1, (('x', 'a', 1), ('x', 'a', 2)): 20, (('x', 'a', 1), ('x', 'b', 1)): 1, (('x', 'a', 1), ('x', 'c', 1)): 1, (('x', 'a', 2), ('x', 'b', 2)): 1, (('x', 'a', 2), ('x', 'c', 2)): 1, (('x', 'b', 0), ('x', 'b', 1)): 20, (('x', 'b', 0), ('x', 'b', 2)): 20, (('x', 'b', 0), ('x', 'd', 0)): 1, (('x', 'b', 1), ('x', 'b', 2)): 20, (('x', 'b', 1), ('x', 'd', 1)): 1, (('x', 'b', 2), ('x', 'd', 2)): 1, (('x', 'c', 0), ('x', 'c', 1)): 20, (('x', 'c', 0), ('x', 'c', 2)): 20, (('x', 'c', 0), ('x', 'd', 0)): 1, (('x', 'c', 1), ('x', 'c', 2)): 20, (('x', 'c', 1), ('x', 'd', 1)): 1, (('x', 'c', 2), ('x', 'd', 2)): 1, (('x', 'd', 0), ('x', 'd', 1)): 20, (('x', 'd', 0), ('x', 'd', 2)): 20, (('x', 'd', 1), ('x', 'd', 2)): 20}"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "objective = objective_terms.get_objective(terms_weights)\n",
    "objective.polynomial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have exactly the same QUBO as in tutorial 02 and 03"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
