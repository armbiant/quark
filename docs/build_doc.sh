#!/bin/bash

#load general information
source ../dev_tools/settings.sh

# create and switch to build directory
mkdir -p build/
cd build

#start spinx-quickstart and set name, author, version and language
echo -e "y\n$PROJECT\n$AUTHORS\n$VERSION\nen\n" | sphinx-quickstart

#move conf which sphinx created
rm make.bat

#start sphinx-autodocumentation  
#sphinx-apidoc -o source/ ../../$PROJECT 

#copy RST conf and tutorials to source directory
cp ../installation.md source/
cp ../getting_started.md source/
cp ../src/*.png source/
cp ../src/*.rst source/
cp ../src/conf.py source/
cp -r ../tutorial source/

#creates the html website
make html 
