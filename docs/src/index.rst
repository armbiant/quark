quark
=====

Mapping real-world combinatorial optimization problems quantum computing interfaces

.. toctree::
    :maxdepth: 2
    :caption: User Documentation

    installation.md
    getting_started.md
    reference.rst

.. toctree::
    :maxdepth: 1
    :caption: Tutorials

    tutorial/Tutorial Part 01 - How to use Polynomials.ipynb
    tutorial/Tutorial Part 02 - How to formulate QUBOs by hand using Polynomials.ipynb
    tutorial/Tutorial Part 03 - How to reuse the QUBO formulation.ipynb
    tutorial/Tutorial Part 04 - How to handle constraints automatically.ipynb
    tutorial/Tutorial Part 05 - How to solve QUBOs.ipynb
