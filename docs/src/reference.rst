Reference
=========

Basic concepts: Polynomials
---------------------------

.. autosummary::
    :toctree: generated/

    quark.Polynomial
    quark.PolyBinary
    quark.PolyIsing
    quark.VariableMapping

Objectives with Constraints
---------------------------

.. autosummary::
    :toctree: generated/

    quark.ConstraintBinary
    quark.ConstrainedObjective

Objective Terms and Objectives
------------------------------

.. autosummary::
    :toctree: generated/

    quark.ObjectiveTerms
    quark.Objective

Solutions of Objectives
-----------------------

.. autosummary::
    :toctree: generated/

    quark.Solution
    quark.ScipModel

Further Hardware-related
------------------------

.. autosummary::
    :toctree: generated/

    quark.HardwareAdjacency
    quark.Embedding
