Quark Developer Guide
=====================

Repository Structure
--------------------

`quark`
* `quark` - the actual source code folder including (abstract) base classes
  + `io` - contains all input/output dedicated functionality as it is separated from the actual functionality
  + `utils` - helper classes to use the quark library
  + `testing` - example implementations of the abstract base classes showing usage of library  
* `tests` - contains the tests, 
  (should have) same hierarchy as quark folder, 
  also good examples on usage of objects
  + `io` - tests dedicated to IO
  + `testdata` - fixed test files used in the tests  
* `docs`
  + `tutorials` - jupyter notebooks showing usage of quark 
  + `src` - source files for sphinx documentation
* `dev_tools` - data for CI and development environment
* `modules` 
  + `development_tools` - git submodule containing tools for development
* `setup` - file to set up development environment (can only be used, if development_tools are initialized)
* `check` - file to check different parts of the code (can only be used, if development_tools are initialized)



Setting up the development environment 
--------------------------------------

by using our [development_tools](https://gitlab.com/quantum-computing-software/tools/development_tools)

**Linux**

0. Check if **Git** is installed, otherwise do 
   ```
   sudo apt-get update
   sudo apt install git
   ``` 
   and clone this repository.

1. Use [development_environment](https://gitlab.com/quantum-computing-software/tools/development_environment) 
   
   :information_source: This will install **miniforge3** (a fork of (Mini-/Ana-)**conda**) and other necessary packages 
   to your local machine. 

   Check if conda is available by typing `conda` in some terminal.
   If it cannot find the command you need to execute
   ```
   echo ". ~/programs/install/miniforge3/etc/profile.d/conda.sh" >> ~/.bashrc
   ``` 
   (resp. other path, if you have changed the default path)
   to make it globally available. 
   Close and reopen the terminal and check it again.  

2. Create a **virtual environment**

   :information_source: It is best practice to use virtual environments, for further information see 
   [Conda Docs](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#).
      
   Make sure the git submodules are initialized by 
   ```bash
   git submodule init
   git submodule update 
   ```   
      
   To create a virtual environment using our tools open a terminal in the quark folder and execute 
    ```bash
    bash setup [local] [--env_name <nondefault_env_name>]
    ```
   For more details and options see in the module [development_tools](https://gitlab.com/quantum-computing-software/tools/development_tools).  

3. Activate the environment with 
   ```
   conda activate quark_dev
   ```
   if it was installed in the conda repository (without local and env_name flag), 
   otherwise replace `quark_dev` with `<nondefault_env_name>` if env_name flag was used, respectively 
   `envs/quark_dev` or `envs/<nondefault_env_name>` if local flag was used.  
   
4. Test if all went fine by executing the checks described in the following section.

**Windows**

The steps correspond to the ones from the Linux installation but the 'machine setup' needs to be done manually. 
   
Afterwards you can follow the steps 2. \- 4. of the Linux setup but using the **Miniforge Prompt** or **Git bash** instead of the Linux terminal. 

(It is recommended to use the local flag for environments as you probably won't have access to the conda installation repository without admin rights.
If you use PyCharm the `envs` folder can cause strange warning, thus it needs to be marked as excluded.)
      

Testing the code
-----------------

We have several code quality checks which can be run with our 
[development_tools](https://gitlab.com/quantum-computing-software/tools/development_tools) by
   ```bash
   bash check pytest
   ```
to see whether the tests run through,
   ```bash
   bash check coverage
   ```
to see whether the test coverage is sufficient or 
   ```bash
   bash check pylint
   ```
to see if the style is conform with our regulations.

If you just run
   ```bash
   bash check 
   ```
you can do the tests all at once. 

These checks are also used in our continuous integration, 
which will be run every time you push something to this repository. 

Automated local checks 
----------------------

It is possible to enforce automated local checks after every commit using git hooks.
The related scripts can be found in the dev_tools/git folder and executed with
   ```bash
   dev_tools/git/add-hooks.sh
   ```
The hooks can be removed with 
   ```bash
   dev_tools/git/remove-hooks.sh
   ```

If you want to commit without using hooks just issue
   ```bash
   git commit --no-verify
   ```


Best Practices for Developers
------------------------------

**Workflow**

* Recognizing a bug or missing feature
* Create issue
  + give possibility to discuss about
  + if urgent ask someone directly or add/mention someone explicitly
  + agree on content and approach of issue
  + if necessary adjust title and description!
* Create branch and merge request from selected issue
  + use gitlab infrastructure
    - follows naming conventions automatically
    - keeps track of you changes
  + good for getting feedback already during programming process
* Work on issue in the merge request
  + keep WIP/draft status
  + if minor questions or problems arise leave comment in merge request
    - if necessary mention someone explicitly with '@username'
    - can also be used to take notes or structure work subtasks with tick boxes 
  + if you recognize another bug or missing feature → create new issue
  + always include meaningful tests to check your code
  + commit code to repository with meaningful messages
* Assign someone else to have a look at your code
  + if you think you made already a big progress or
  + if you run into wholes and need help
  + wait until you are reassigned
  + probably discuss options
* Iterate last two steps if necessary
* Finalize
  + resolve all remarks of the reviewer 
  + make sure the tests run and you fulfil the required coverage and pylint scores!
  + if merge conflicts might appear merge master into your branch 
* Done
  + resolve WIP status
  + assign to approver
  + wait for approval and merge
