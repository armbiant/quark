Installation
------------

1. Download or clone this repository.

2. We recommend to set up a virtual python environment (e.g. with [miniforge](https://github.com/conda-forge/miniforge))
    ```
    conda create -n quark_install python=3.10
    conda activate quark_install
    ```

3. To install the requirements of quark execute inside the top repository
    ```
    conda config --add channels conda-forge
    conda install --file ./requirements.txt
    ```

4. To install quark itself execute inside the top repository
    ```
    conda install conda-build
    conda develop ./
    ```

5. To test the installation run 
    ```
    python
    ```
    and execute 
    ```
    >>> import quark
    ```
    if this works without any output, the installation should be fine. 
