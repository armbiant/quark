# Copyright 2020 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the ConstrainedObjective by inherited example and without """

import itertools
import pytest

from quark import PolyBinary, PolyIsing, ConstraintBinary, ConstrainedObjective
from quark.testing import ExampleInstance, ExampleConstrainedObjective, ExampleObjectiveTerms
from quark.testing.example_objective_terms import X, ONE_COLOR_PER_NODE, COLORED_EDGES


VALID_SOLUTION = {(X, 1, 0): 0, (X, 2, 0): 1, (X, 3, 0): 0, (X, 4, 0): 1,
                  (X, 1, 1): 1, (X, 2, 1): 0, (X, 3, 1): 1, (X, 4, 1): 0}
INVALID_SOLUTION = {(X, 1, 0): 0, (X, 2, 0): 1, (X, 3, 0): 0, (X, 4, 0): 1,
                    (X, 1, 1): 1, (X, 2, 1): 1, (X, 3, 1): 1, (X, 4, 1): 0}
OBJECTIVE_POLY_FLAT_STR = "x0 x2 + x2 x4 + x2 x6 + x4 x6 + x1 x3 + x3 x5 + x3 x7 + x5 x7"

@pytest.fixture(name="instance")
def fixture_instance():
    """ provide test instance """
    edges = [(1, 2), (2, 3), (3, 4), (2, 4)]
    num_colors = 2
    yield ExampleInstance(edges, num_colors)

@pytest.fixture(name="constrained_objective")
def fixture_constrained_objective(instance):
    """ provide test constrained objective """
    yield ExampleConstrainedObjective(instance=instance)

@pytest.fixture(name="objective_terms")
def fixture_objective_terms(instance):
    """ provide test objective terms """
    yield ExampleObjectiveTerms(instance=instance)


def test_init(constrained_objective):
    """ test constrained objective initialization """
    exp_constraints = {ONE_COLOR_PER_NODE + f"_{n}":
                           ConstraintBinary(PolyBinary({((X, n, 0),): 1, ((X, n, 1),): 1}), 1, 1)
                       for n in range(1, 5)}
    exp_objective_poly = PolyBinary.read_from_string(OBJECTIVE_POLY_FLAT_STR)
    assert dict(constrained_objective) == exp_constraints
    assert constrained_objective.objective_poly.compact() == exp_objective_poly

def test_get_objective_terms(constrained_objective):
    """ test objective terms creation """
    exp_objective_poly = PolyBinary.read_from_string(OBJECTIVE_POLY_FLAT_STR)
    exp_objective_terms = {ONE_COLOR_PER_NODE + f"_{n}":
                               PolyBinary({(): 1, ((X, n, 0),): -1, ((X, n, 1),): -1, ((X, n, 0), (X, n, 1)): 2})
                           for n in range(1, 5)}
    objective_terms = constrained_objective.get_objective_terms(COLORED_EDGES)
    for node_name in exp_objective_terms:
        assert objective_terms[node_name] == exp_objective_terms[node_name]
    assert objective_terms[COLORED_EDGES].compact() == exp_objective_poly

def test_get_objective_terms_no_objective(constrained_objective):
    """ test objective terms creation without explicit objective polynomial """
    constrained_objective.objective_poly = PolyBinary()
    objective_terms = constrained_objective.get_objective_terms(COLORED_EDGES)
    assert not COLORED_EDGES in objective_terms

def test_get_broken_constraints(constrained_objective):
    """ test broken constraints """
    assert constrained_objective.get_broken_constraints(VALID_SOLUTION) == []
    assert constrained_objective.get_broken_constraints(INVALID_SOLUTION) == [ONE_COLOR_PER_NODE + "_2"]

def test_check_validity(constrained_objective):
    """ test validity of solution """
    assert constrained_objective.check_validity(VALID_SOLUTION)
    assert not constrained_objective.check_validity(INVALID_SOLUTION)

def test_extract(constrained_objective):
    """ test solution extraction """
    assert constrained_objective.get_original_problem_solution(VALID_SOLUTION) == {2: 0, 4: 0, 1: 1, 3: 1}

def test_get_objective_terms_no_impl():
    """ test objective terms creation with constraint initialization """
    objective_poly = PolyBinary.read_from_string(OBJECTIVE_POLY_FLAT_STR)
    constraint = ConstraintBinary(PolyBinary.read_from_string("x0"), 1, 1)
    constraints = {ONE_COLOR_PER_NODE : constraint}
    constrained_objective = ConstrainedObjective(objective_poly, constraints)
    assert constrained_objective.objective_poly == objective_poly
    assert dict(constrained_objective) == constraints
    assert constrained_objective != ConstrainedObjective(PolyBinary({"x0" : 1}), constraints)
    assert constrained_objective != constraints

    with pytest.raises(NotImplementedError, match="Provide 'objective_poly' on initialization or "):
        ConstrainedObjective()
    with pytest.raises(NotImplementedError, match="Provide 'constraints' on initialization or "):
        ConstrainedObjective(objective_poly)
    with pytest.raises(ValueError, match="Currently only binary objective functions are supported for the"):
        ConstrainedObjective(PolyIsing({"x0" : 1}), constraints)

def test_objective_terms_two_paths_constraint_and_objective_terms(constrained_objective, objective_terms):
    """ test objective terms creation by comparing it to constrained_objective """
    objective_terms_from_co = constrained_objective.get_objective_terms(COLORED_EDGES, combine_prefixes=())
    one_color_per_node_polys = dict(itertools.islice(objective_terms_from_co.items(), 4))
    one_color_per_node_sum_poly = sum(one_color_per_node_polys.values())
    assert objective_terms_from_co[COLORED_EDGES] == objective_terms[COLORED_EDGES]
    assert one_color_per_node_sum_poly == objective_terms[ONE_COLOR_PER_NODE]

    ots_from_co_combined = constrained_objective.get_objective_terms(COLORED_EDGES, combine_prefixes=ONE_COLOR_PER_NODE)
    assert ots_from_co_combined != objective_terms_from_co
    assert ots_from_co_combined == objective_terms
