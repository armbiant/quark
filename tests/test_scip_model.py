# Copyright 2020 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing scip_model """

import os
from itertools import product
import pytest

from quark import Objective, Polynomial, PolyBinary, PolyIsing, ScipModel, ConstrainedObjective, ConstraintBinary
from quark.scip_model import QUADRATIC_OBJECTIVE, OPTIMAL
from quark.testing import ExampleInstance, ExampleConstrainedObjective
from quark.testing.example_constrained_objective import X


TEST_DIR = os.path.dirname(os.path.abspath(__file__)) + "/"
FILENAME_TMP = TEST_DIR + "scip_model.cip"
FILENAME_FIX = TEST_DIR + "testdata/scip_model.cip"
POLY_ISING = PolyIsing({(("x", 1),) : 1, (("x", 1), ("x", 2)) : -1}) + 5
OBJECTIVE = Objective(POLY_ISING, "test_ising")

POLY = Polynomial.read_from_string("- 2 x0 x4 - 3 x0 x5 + x0 x6 + 3 x0 x7 + 4 x1 x4 "
                                   "+ 3 x1 x6 + 4 x0 - 3 x1 - 4 x0 - 5 x2 - 4 x5 + 16")

@pytest.fixture(name="scip_model")
def fixture_scip_model():
    """ provide scip model for testing """
    yield ScipModel.get_from_objective(OBJECTIVE)

def test_get_from_objective(scip_model):
    """ test solution of scip model created from objective """
    constraint = scip_model.getConss()[0]
    assert list(scip_model.data.keys()) == [("x", 1), ("x", 2)]
    assert constraint.name == QUADRATIC_OBJECTIVE
    assert constraint.isNonlinear()

    scip_model.optimize()
    assert scip_model.getObjVal() == 3
    assert scip_model.getVal(scip_model.data[("x", 1)]) == 0
    assert scip_model.getVal(scip_model.data[("x", 2)]) == 0

def test_get_from_constrained_objective():
    """ test solution of scip model created from objective """
    instance = ExampleInstance([(0, 1), (1, 2), (2, 0)], 2)
    constrained_objective = ExampleConstrainedObjective(instance=instance)
    scip_model = ScipModel.get_from_constrained_objective(constrained_objective)
    constraints = scip_model.getConss()
    assert list(scip_model.data.keys()) == [(X, 0, 0), (X, 0, 1), (X, 1, 0), (X, 1, 1), (X, 2, 0), (X, 2, 1)]
    assert len(constraints) == 4
    assert constraints[0].name == QUADRATIC_OBJECTIVE
    assert constraints[0].isNonlinear()

    for node in range(3):
        assert constraints[node + 1].name == f"one_color_per_node_{node}"
        assert constraints[node + 1].isLinear()

    scip_model.optimize()
    assert scip_model.getObjVal() == 1

def test_get_from_manual_constrained_objective():
    """ test solution of scip model created from objective """
    objective_poly = PolyBinary({(0,): 1, (1,): -1})
    constraint = ConstraintBinary(PolyBinary({(0,): 1}), 0, 1)
    constrained_objective = ConstrainedObjective(objective_poly=objective_poly, constraints={"useless": constraint})
    scip_model = ScipModel.get_from_constrained_objective(constrained_objective)
    solution = scip_model.solve(**{"limits/nodes": 100})
    assert scip_model.getObjVal() == -1
    assert solution == {0: 0, 1: 1}

def test_get_from_manual_constrained_objective_infeasible():
    """ test solution of scip model created from objective """
    objective_poly = PolyBinary({(0,): 1, (1,): -1})
    constraint = ConstraintBinary(PolyBinary({(0,): 1}), 2, 3)
    constrained_objective = ConstrainedObjective(objective_poly=objective_poly, constraints={"infeasible": constraint})
    scip_model = ScipModel.get_from_constrained_objective(constrained_objective)
    solution = scip_model.solve()
    assert not solution
    assert solution.solving_status == "infeasible"

    with pytest.raises(ValueError, match="Cannot get value for variable '2'"):
        scip_model.get_value(2)
    scip_model.data.update({2: 1})
    assert scip_model.get_value(2) == 1

def test_solve():
    """ test solution of different objectives """
    _assert_solve(Objective(PolyBinary(POLY), "test_objective_binary"))
    _assert_solve(Objective(PolyIsing(POLY), "test_objective_ising"))
    _assert_solve(Objective(PolyIsing(POLY).invert(), "test_objective_ising_inverted"))

def _assert_solve(objective):
    model = ScipModel.get_from_objective(objective)
    solution = model.solve()
    assert solution.solving_status == OPTIMAL

    score_evaluated = objective.polynomial.evaluate(solution)
    assert solution.objective_value == score_evaluated

    var_domain = [-1, 1] if objective.is_ising() else [0, 1]
    best_score, best_assignments = _minimize_polynomial_brute_force(objective.polynomial, var_domain)
    assert solution.objective_value == best_score
    assert solution in best_assignments

def _minimize_polynomial_brute_force(poly: Polynomial, var_domain):
    domains = [var_domain] * len(poly.variables)
    min_score = float("infinity")
    min_assignments = []
    for values in product(*domains):
        assignment = dict(zip(poly.variables, values))
        score = poly.evaluate(assignment)
        if score < min_score:
            min_score = score
            min_assignments = []
        if score == min_score:
            min_assignments.append(assignment)
    return min_score, min_assignments

def test_load_cip(scip_model):
    """ test loading of SCIP model """
    with pytest.warns(UserWarning, match="Cannot retrieve value for 'from_inverted_ising', will use 'None'"):
        loaded = ScipModel.load_cip(FILENAME_FIX)
    scip_model.from_inverted_ising = None
    _check_model(loaded, scip_model)

def test_io_cip(scip_model):
    """ test IO round trip """
    scip_model.save_cip(FILENAME_TMP)
    loaded = scip_model.load_cip(FILENAME_TMP)
    _check_model(loaded, scip_model)
    os.remove(FILENAME_TMP)

def _check_model(loaded, scip_model):
    assert loaded.getProbName() == scip_model.getProbName()
    assert loaded.data.keys() == scip_model.data.keys()
    assert loaded.from_inverted_ising == scip_model.from_inverted_ising

    loaded_constraint = loaded.getConss()[0]
    assert loaded_constraint.isNonlinear()
    assert loaded_constraint.name == QUADRATIC_OBJECTIVE

    loaded_objective = loaded.getObjective()
    objective = scip_model.getObjective()
    loaded_vars = [term[0].name for term in loaded_objective.terms]
    objective_vars = [term[0].name for term in objective.terms]
    assert set(loaded_vars) == set(objective_vars)
