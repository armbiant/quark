# Copyright 2020 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the ConstraintBinary """

import pytest

from quark import PolyBinary, PolyIsing, ConstraintBinary
from quark.constraint_binary import get_binary_representation_polynomial, get_linear_inequality_penalty_polynomial


QUADRATIC_POLY = PolyBinary({(("x", 1),): 7, (("x", 2), ("x", 1)): 2, (): 3})
LINEAR_POLY = PolyBinary({(("x", 1),): 7, (("x", 2),): 2})
NAME = "test_constraint"


def test_get_linear_inequality_penalty_polynomial():
    """ test the creation of the penalty term for linear polynomial """
    exp_poly = LINEAR_POLY ** 2
    assert get_linear_inequality_penalty_polynomial(LINEAR_POLY, 0) == exp_poly

    exp_poly = (LINEAR_POLY - PolyBinary({(("slack", 0),) : 1})) ** 2
    assert get_linear_inequality_penalty_polynomial(LINEAR_POLY, 1) == exp_poly

    with pytest.raises(ValueError, match="Method only makes sense for linear polynomials"):
        get_linear_inequality_penalty_polynomial(QUADRATIC_POLY, 0)

def test_consistency():
    """ test errors for inconsistent data """
    with pytest.raises(ValueError, match="Both bounds need to be given, unbounded constraints cannot be handled"):
        ConstraintBinary(PolyBinary(), 4, None)
    with pytest.raises(ValueError, match="Lower bound must be less than or equal to upper bound"):
        ConstraintBinary(PolyBinary(), 4, 2)
    with pytest.raises(ValueError, match="Unfulfillable constraint '5 == 1'"):
        ConstraintBinary(PolyBinary({() : 5}), 1, 1)
    with pytest.warns(UserWarning, match="Useless constraint '0 <= 5 <= 10'"):
        ConstraintBinary(PolyBinary({() : 5}), 0, 10)
    with pytest.raises(ValueError, match="At the moment can only handle PolyBinary"):
        ConstraintBinary(PolyIsing(), 0, 1)

def test_repr():
    """ test string representation """
    constraint = ConstraintBinary(PolyBinary({(1,) : 5}), 0, 10)
    assert repr(constraint) == "('{(1,): 5}', 0, 10)"

def test_add():
    """ test addition of scalars to constraint """
    poly = PolyBinary({(("x", 1),): 7, (("x", 2), ("x", 1)): 2, (): 3})
    exp_poly = PolyBinary({(("x", 1),): 7, (("x", 2), ("x", 1)): 2, (): 5})
    constraint = ConstraintBinary(poly, 5, 10)
    constraint += 2
    assert constraint.polynomial == exp_poly
    assert constraint.lower_bound == 7
    assert constraint.upper_bound == 12

def test_sub():
    """ test subtraction of scalars from constraint """
    poly = PolyBinary({(("x", 1),): 7, (("x", 2), ("x", 1)): 2, (): 3})
    exp_poly = PolyBinary({(("x", 1),): 7, (("x", 2), ("x", 1)): 2, (): 1})
    constraint = ConstraintBinary(poly, 5, 10)
    constraint -= 2
    assert constraint.polynomial == exp_poly
    assert constraint.lower_bound == 3
    assert constraint.upper_bound == 8

def test_mul():
    """ test multiplication of scalars to constraint """
    poly = PolyBinary({(("x", 1),): 7, (("x", 2), ("x", 1)): 2, (): 3})
    exp_poly = PolyBinary({(("x", 1),): 14, (("x", 2), ("x", 1)): 4, (): 6})
    constraint = ConstraintBinary(poly, 5, 10)
    constraint *= 2
    assert constraint.polynomial == exp_poly
    assert constraint.lower_bound == 10
    assert constraint.upper_bound == 20

def test_eq():
    """ test equality of constraints """
    poly1 = PolyBinary({(("x", 1),): 7, (("x", 2), ("x", 1)): 2, (): 3})
    constraint1 = ConstraintBinary(poly1, 5, 10)
    constraint2 = ConstraintBinary(poly1, 5, 10)
    assert constraint1 != poly1
    assert constraint1 == constraint2
    assert constraint1 == constraint2.get_normalized()
    assert constraint1.polynomial != constraint2.get_normalized().polynomial
    assert constraint1 == constraint1 + 3
    assert constraint1 == constraint1 * 4

    polys = [PolyBinary({(("x", 1),): 5, (("x", 2), ("x", 1)): 2, (): 3}), poly1, poly1]
    lowers = [5, 6, 5]
    uppers = [10, 10, 8]
    for i in range(3):
        assert constraint1 != ConstraintBinary(polys[i], lowers[i], uppers[i])

def test_get_linear_inequality_term():
    """ test construction of penalty term for linear inequality """
    constraint = ConstraintBinary(LINEAR_POLY, 0, 0)
    exp_poly = LINEAR_POLY ** 2
    exp_penalty_terms = {NAME : exp_poly}
    assert constraint.get_linear_inequality_penalty_term(NAME) == exp_penalty_terms

    constraint = ConstraintBinary(LINEAR_POLY, 0, 1)
    exp_poly = (LINEAR_POLY - PolyBinary({(("test_constraint_slack", 0),) : 1})) ** 2
    exp_penalty_terms = {NAME : exp_poly}
    assert constraint.get_linear_inequality_penalty_term(NAME) == exp_penalty_terms

def test_reduce_quadratic_to_linear():
    """ test reduction to linear polynomial """
    constraint = ConstraintBinary(LINEAR_POLY, 3, 10)
    linear_constraint, reductions = constraint.reduce_to_linear()
    assert linear_constraint == constraint
    assert not reductions

    constraint = ConstraintBinary(QUADRATIC_POLY, 3, 10)
    linear_constraint, reductions = constraint.reduce_to_linear()
    exp_poly = PolyBinary({(("reduction", "x", 1, "x", 2),): 2, (("x", 1),): 7, (): 3})
    exp_constraint = ConstraintBinary(exp_poly, 3, 10)
    exp_reductions = {'reduction_x_1_x_2': PolyBinary({(('reduction', 'x', 1, 'x', 2),): 3,
                                                       (('reduction', 'x', 1, 'x', 2), ('x', 1)): -2,
                                                       (('reduction', 'x', 1, 'x', 2), ('x', 2)): -2,
                                                       (('x', 1), ('x', 2)): 1})}
    assert linear_constraint == exp_constraint
    assert reductions == exp_reductions

def test_get_objective_terms_constant_polynomial():
    """ test construction of objective terms with constant polynomial """
    poly = PolyBinary({(): 5})
    with pytest.warns(UserWarning, match="Useless constraint"):
        constraint = ConstraintBinary(poly, 5, 5)
        assert not constraint.get_penalty_terms()
        constraint = ConstraintBinary(poly, 0, 10)
        assert not constraint.get_penalty_terms()

def test_get_objective_terms_linear_equality_constraint():
    """ test construction of objective terms for equality constraint """
    constraint = ConstraintBinary(LINEAR_POLY, 5, 5)
    exp_penalty_terms = {NAME: (LINEAR_POLY - 5)**2}
    assert constraint.get_penalty_terms(NAME) == exp_penalty_terms

    # in the following case the polynomial can be used itself as a penalty term as it is >=0 for all assignments
    constraint = ConstraintBinary(LINEAR_POLY, 0, 0)
    exp_penalty_terms = {NAME: LINEAR_POLY}
    assert constraint.get_penalty_terms(NAME) == exp_penalty_terms

    with pytest.raises(ValueError, match="Polynomial with non-negative coefficients shall sum up to negative value"):
        constraint = ConstraintBinary(LINEAR_POLY, -5, -5)
        constraint.get_penalty_terms(NAME)

def test_get_objective_terms_linear_inequality_constraint():
    """ test construction of objective terms for inequality """
    constraint = ConstraintBinary(LINEAR_POLY, 3, 10)
    exp_poly = (LINEAR_POLY - 3 - get_binary_representation_polynomial(7, "test_constraint_slack")) ** 2
    exp_penalty_terms = {NAME: exp_poly}
    assert constraint.get_penalty_terms(NAME) == exp_penalty_terms

def test_get_objective_terms_quadratic_equality_constraint():
    """ test construction of objective terms for equality constraint with quadratic polynomial """
    constraint = ConstraintBinary(QUADRATIC_POLY, 5, 5)
    poly, exp_reductions = QUADRATIC_POLY.reduce()
    exp_penalty_terms = {NAME: (poly - 5) ** 2}
    exp_penalty_terms.update(exp_reductions)
    assert constraint.get_penalty_terms(NAME) == exp_penalty_terms

    # in the following case the polynomial (minus the constant) can be used itself as a penalty term
    # as it is >=0 for all assignments
    constraint = ConstraintBinary(QUADRATIC_POLY, 3, 3)
    exp_penalty_terms = {NAME: QUADRATIC_POLY - 3}
    assert constraint.get_penalty_terms(NAME) == exp_penalty_terms

    with pytest.raises(ValueError, match="Polynomial with non-negative coefficients shall sum up to negative value"):
        constraint = ConstraintBinary(QUADRATIC_POLY, 0, 0)
        constraint.get_penalty_terms(NAME)

def test_get_objective_terms_quadratic_inequality_constraint():
    """ test construction of objective terms for inequality with quadratic polynomial """
    constraint = ConstraintBinary(QUADRATIC_POLY, 5, 10)
    poly, exp_reductions = QUADRATIC_POLY.reduce()
    exp_poly = (poly - 5 - get_binary_representation_polynomial(5, "test_constraint_slack")) ** 2
    exp_penalty_terms = {NAME: exp_poly}
    exp_penalty_terms.update(exp_reductions)
    assert constraint.get_penalty_terms(NAME) == exp_penalty_terms

def test_errors():
    """ test consistency """
    with pytest.raises(ValueError, match="Constant must be a number"):
        _ = ConstraintBinary(LINEAR_POLY, 0, 0) + PolyBinary()
    with pytest.raises(ValueError, match="Constant must be a number"):
        _ = ConstraintBinary(LINEAR_POLY, 0, 0) * PolyBinary()

    with pytest.raises(ValueError, match="Inequality constraint must have integer coefficients and boundaries"):
        ConstraintBinary(PolyBinary({"x" : 3.5}), 0, 1).get_penalty_terms()
    with pytest.raises(ValueError, match="Inequality constraint must have integer coefficients and boundaries"):
        ConstraintBinary(PolyBinary({"x" : 3}), 0, 1.5).get_penalty_terms()

def test_to_string():
    """ test string creation """
    constraint = ConstraintBinary(QUADRATIC_POLY, 1, 2)
    assert constraint.to_string() == "1 <= +3 +7 x_1 +2 x_1 x_2 <= 2"

    constraint = ConstraintBinary(QUADRATIC_POLY, 4, 4)
    assert constraint.to_string() == "+3 +7 x_1 +2 x_1 x_2 == 4"
