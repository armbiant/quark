# Copyright 2020 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the ObjectiveTerms by inherited example and without """

import pytest

from quark import Polynomial, PolyBinary, PolyIsing, ObjectiveTerms, Objective
from quark.testing import ExampleInstance, ExampleObjectiveTerms
from quark.testing.example_objective_terms import X, COLORED_EDGES, ONE_COLOR_PER_NODE
from .test_constrained_objective import VALID_SOLUTION, INVALID_SOLUTION


@pytest.fixture(name="objective_terms")
def fixture_objective_terms():
    """ provide the objective terms for testing """
    edges = [(1, 2), (2, 3), (3, 4), (2, 4)]
    num_colors = 2
    instance = ExampleInstance(edges, num_colors)
    yield ExampleObjectiveTerms(instance=instance)

@pytest.fixture(name="objective_terms_different_types")
def fixture_objective_terms_different_types():
    """ test objective terms with different types """
    poly_ising = PolyIsing({(0, 1): 2, (2, 3): 4})
    poly_binary = PolyBinary({(0, 1): 2, (2, 3): 4})
    poly_none = Polynomial({(1, 2): 3})
    terms = {"ising": poly_ising, "binary": poly_binary, "none": poly_none}
    yield ExampleObjectiveTerms(objective_terms=terms, constraint_terms_names=["ising", "binary"])


def test_get_string(objective_terms):
    """ test if unique string is generated correctly """
    terms_weights = {COLORED_EDGES : 1.0, ONE_COLOR_PER_NODE : 2.4}
    exp_str = "colored_edges1.000000e+00_one_color_per_node2.400000e+00"
    assert objective_terms.get_objective_name(terms_weights) == exp_str

def test_init(objective_terms):
    """ test objective terms creation """
    exp_colored_edges = PolyBinary({((X, 1, 0), (X, 2, 0)): 1,
                                    ((X, 2, 0), (X, 3, 0)): 1,
                                    ((X, 2, 0), (X, 4, 0)): 1,
                                    ((X, 3, 0), (X, 4, 0)): 1,
                                    ((X, 1, 1), (X, 2, 1)): 1,
                                    ((X, 2, 1), (X, 3, 1)): 1,
                                    ((X, 2, 1), (X, 4, 1)): 1,
                                    ((X, 3, 1), (X, 4, 1)): 1})
    assert objective_terms[COLORED_EDGES] == exp_colored_edges

    exp_one_color_per_node = PolyBinary({(): 4,
                                         ((X, 1, 0),): -1,
                                         ((X, 1, 1),): -1,
                                         ((X, 2, 0),): -1,
                                         ((X, 2, 1),): -1,
                                         ((X, 3, 0),): -1,
                                         ((X, 3, 1),): -1,
                                         ((X, 4, 0),): -1,
                                         ((X, 4, 1),): -1,
                                         ((X, 1, 0), (X, 1, 1)): 2,
                                         ((X, 2, 0), (X, 2, 1)): 2,
                                         ((X, 3, 0), (X, 3, 1)): 2,
                                         ((X, 4, 0), (X, 4, 1)): 2})
    assert objective_terms[ONE_COLOR_PER_NODE] == exp_one_color_per_node

def test_get_objective(objective_terms):
    """ test objective creation """
    terms_weights = {COLORED_EDGES : 1, ONE_COLOR_PER_NODE : 2}
    exp_poly = PolyBinary.read_from_string("1 x0 x2 + 1 x2 x4 + 1 x4 x6 +1 x2 x6 +1 x1 x3 +1 x3 x5 +1 x5 x7"+
                                           "+1 x3 x7 + -2 x0 +4 x0 x1 -2 x1 -2 x2 +4 x2 x3 -2 x3 -2 x4 "+
                                           "+4 x4 x5 -2 x5 -2 x6 +4 x6 x7 -2 x7 + 8")
    poly = objective_terms.get_weighted_sum_polynomial(terms_weights)
    assert isinstance(poly, PolyBinary)
    assert poly.compact() == exp_poly

    exp_str = "colored_edges1.000000e+00_one_color_per_node2.000000e+00"
    exp_objective = Objective(polynomial=poly, name=exp_str)
    objective = objective_terms.get_objective(terms_weights)
    assert isinstance(objective.polynomial, PolyBinary)
    assert objective == exp_objective

    with pytest.raises(ValueError, match="There is a mismatch between given and stored objective_terms names"):
        objective_terms.get_objective({"wrong_pw" : 4})
    with pytest.raises(ValueError, match="Cannot add PolyIsing and PolyBinary"):
        objective_terms.get_weighted_sum_polynomial({COLORED_EDGES : 1.0, ONE_COLOR_PER_NODE : PolyIsing()})

def test_evaluate_terms(objective_terms):
    """ test check solution """
    values = objective_terms.evaluate_terms(VALID_SOLUTION)
    assert values[COLORED_EDGES] == 1.0
    assert values[ONE_COLOR_PER_NODE] == 0.0

def test_check_validity(objective_terms):
    """ test check solution """
    assert objective_terms.check_validity(VALID_SOLUTION)
    assert not objective_terms.check_validity(INVALID_SOLUTION)

def test_objective_terms_creation_no_impl():
    """ test objective terms creation """
    colored_edges = PolyBinary.read_from_string("5 + x0 x1 + x1 x2 + x2 x3 + x0 x3 + x0 x2")
    one_color_per_node = PolyBinary.read_from_string("1 - x0")
    objective_terms_dict = dict(colored_edges=colored_edges, one_color_per_node=one_color_per_node)
    constraint_terms_names = [ONE_COLOR_PER_NODE]
    objective_terms = ObjectiveTerms(objective_terms_dict, constraint_terms_names)
    assert objective_terms.constraint_terms_names == constraint_terms_names
    assert dict(objective_terms) == objective_terms_dict

    # explicitly setting constraint terms names to [] is fine
    objective_terms2 = ObjectiveTerms(objective_terms_dict, [])
    assert objective_terms2.constraint_terms_names == []
    assert dict(objective_terms2) == objective_terms_dict
    assert objective_terms2 != objective_terms

    with pytest.raises(NotImplementedError, match="Provide 'objective_terms' on initialization or "):
        ObjectiveTerms()
    with pytest.raises(NotImplementedError, match="Provide 'constraint_terms_names' on initialization or "):
        ObjectiveTerms(objective_terms_dict)
    with pytest.raises(ValueError, match="All constraint terms names should also be a key of the objective terms"):
        ObjectiveTerms(objective_terms_dict, ["wrong_constraint_name"])

def test_all_to_binary(objective_terms_different_types):
    """ test transformation of all terms to binary polynomials """
    objective_terms_binary = objective_terms_different_types.all_to_binary()
    exp_transformed_ising = PolyBinary({(): 6, (0,): -4, (0, 1): 8, (1,): -4, (2,): -8, (2, 3): 16, (3,): -8})
    assert isinstance(objective_terms_binary, ExampleObjectiveTerms)
    assert all(isinstance(poly, PolyBinary) for poly in objective_terms_binary.values())
    assert objective_terms_binary["binary"] == objective_terms_different_types["binary"]
    assert objective_terms_binary["none"] == objective_terms_different_types["none"]
    assert objective_terms_binary["ising"] == exp_transformed_ising

def test_all_to_ising(objective_terms_different_types):
    """ test conversion of all terms to Ising polynomials """
    # without inversion of ising
    objective_terms_ising = objective_terms_different_types.all_to_ising()
    assert isinstance(objective_terms_ising, ExampleObjectiveTerms)
    assert all(isinstance(poly, PolyIsing) for poly in objective_terms_ising.values())
    assert all(not poly.is_inverted() for poly in objective_terms_ising.values())

    # with inversion of ising
    objective_terms_ising = objective_terms_different_types.all_to_ising(inverted=True)
    assert isinstance(objective_terms_ising, ExampleObjectiveTerms)
    assert all(isinstance(poly, PolyIsing) for poly in objective_terms_ising.values())
    assert all(poly.is_inverted() for poly in objective_terms_ising.values())

def test_get_default_terms_weights(objective_terms):
    """ test the default weights """
    exp_weights = {"colored_edges": 1, "one_color_per_node": 1}
    assert objective_terms.get_default_terms_weights() == exp_weights

    exp_weights = {"colored_edges": 1, "one_color_per_node": 3}
    assert objective_terms.get_default_terms_weights(penalty_scale=3) == exp_weights
