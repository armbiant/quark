# Copyright 2020 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" test binary polynomial module """

from itertools import product
import pytest
import numpy as np

from quark import Polynomial, PolyBinary, PolyIsing
from quark.poly_binary import get_reduction_penalty_polynomial, get_binary_representation_polynomial, BETTER, BEST


P1_DICT = {(1,): 5, (2,): 6, (1, 3): -3, (2, 4): 3, (4,): -1, (): 3}
P2_DICT = {(("x", 1), ("y", 2, 3)): -2, (("y", 1, 2),): 3}
P3_DICT = {(0,): 5, (1, 1): 6, (2, 0): -3, (1, 3): 0.5, (3,): -1, (0, 1): 0.9, (): 16, (2,): 0}
P1_ISING_DICT = {(): 8.0, (1,): 1.75, (1, 3): -0.75, (2,): 3.75, (2, 4): 0.75, (3,): -0.75, (4,): 0.25}
P1_ISING_INV_DICT = {(): 8.0, (1,): -1.75, (1, 3): -0.75, (2,): -3.75, (2, 4): 0.75, (3,): 0.75, (4,): -0.25}

POLY1 = PolyBinary(P1_DICT)
POLY2 = PolyBinary(P2_DICT)
POLY3 = PolyBinary(P3_DICT)
POLY1_NONE = Polynomial(P1_DICT)
POLY1_ISING = PolyIsing(P1_ISING_DICT)
POLY1_ISING_INV = PolyIsing(P1_ISING_INV_DICT, inverted=True)

POLY3_QUADRATIC = [[ 5. ,  0.9, -3. ,  0. ],
                   [ 0. ,  6. ,  0. ,  0.5],
                   [ 0. ,  0. ,  0. ,  0. ],
                   [ 0. ,  0. ,  0. , -1. ]]
POLY3_LINEAR = [ 5.,  6.,  0., -1.]
POLY3_QUAD_NO_DIAG = [[ 0. ,  0.9, -3. ,  0. ],
                      [ 0. ,  0. ,  0. ,  0.5],
                      [ 0. ,  0. ,  0. ,  0. ],
                      [ 0. ,  0. ,  0. ,  0. ]]

QUADRATIC_POLY = PolyBinary({(("x", 1),): 7, (("x", 2), ("x", 1)): 2, (): 3})
LINEAR_POLY = PolyBinary({(("x", 1),): 7, (("x", 2),): 2})

FROM_RIGHT_WARNING = "PolyBinary will be cast to Polynomial when added or multiplied from the right"


def test_init():
    """ test creation """
    poly = PolyBinary({(1, 1, 1): 5, (2,): 9, (2, 2) : -3, (3, 3, 1): -3, (2, 4): 3, (4,): -1, (): 3})
    assert POLY1 == poly
    assert P1_DICT == dict(poly)

    poly = PolyBinary({(("y", 2, 3), ("x", 1), ("y", 2, 3), ("y", 2, 3)): -2, (("y", 1, 2),): 3})
    assert POLY2 == poly
    assert P2_DICT == dict(poly)

def test_add():
    """ test addition """
    # pylint: disable=unidiomatic-typecheck
    sum_poly = PolyBinary(P1_DICT)
    sum_poly += Polynomial({(0,) : 1})
    assert type(POLY1 + Polynomial({(0,) : 1})) is PolyBinary
    assert type(sum_poly) is PolyBinary

    with pytest.warns(UserWarning, match=FROM_RIGHT_WARNING):
        sum_poly = Polynomial({(4,): 1}) + POLY1
        assert type(sum_poly) is Polynomial

    with pytest.warns(UserWarning, match=FROM_RIGHT_WARNING):
        sum_poly = Polynomial({(4,): 1})
        sum_poly += POLY1
        assert type(sum_poly) is Polynomial

    with pytest.raises(ValueError, match="Cannot add PolyBinary and PolyIsing"):
        _ = POLY1 + PolyIsing(P1_DICT)

def test_mul():
    """ test multiplication """
    # pylint: disable=unidiomatic-typecheck
    exp_poly = PolyBinary({(1, 4): 5, (2, 4): 9, (1, 3, 4): -3, (4,): 2})
    prod_poly = POLY1 * PolyBinary({(4,): 1})
    assert prod_poly == exp_poly

    exp_poly = PolyBinary({(("x", 1), ("y", 2, 3)): -4, (("x", 1), ("y", 1, 2)): 6})
    prod_poly = POLY2 * PolyBinary({(("x", 1),): 2})
    assert prod_poly == exp_poly

    exp_poly = PolyBinary({(("x", 1), ("y", 2, 3)): 4, (("y", 1, 2),): 9, (("x", 1), ("y", 2, 3), ("y", 1, 2)) : -12})
    prod_poly = POLY2 * POLY2
    assert prod_poly == exp_poly

    prod_poly = PolyBinary(P1_DICT)
    prod_poly *= Polynomial({(0,) : 1})
    assert type(POLY1 * Polynomial({(0,) : 1})) is PolyBinary
    assert type(prod_poly) is PolyBinary

    with pytest.warns(UserWarning, match=FROM_RIGHT_WARNING):
        prod_poly = Polynomial({(4,): 1}) * POLY1
        assert type(prod_poly) is Polynomial

    with pytest.warns(UserWarning, match=FROM_RIGHT_WARNING):
        prod_poly = Polynomial({(4,): 1})
        prod_poly *= POLY1
        assert type(prod_poly) is Polynomial

    with pytest.raises(ValueError, match="Cannot multiply PolyBinary and PolyIsing"):
        _ = POLY1 * PolyIsing(P1_DICT)

def test_copy():
    """ test copying """
    # pylint: disable=unidiomatic-typecheck
    poly1_copy = POLY1.copy()
    assert poly1_copy == POLY1
    assert dict(poly1_copy) == dict(POLY1)
    assert type(poly1_copy) is PolyBinary

    p2_copy = POLY2.copy()
    assert p2_copy == POLY2
    assert dict(p2_copy) == dict(POLY2)
    assert type(poly1_copy) is PolyBinary

def test_remove_zero_coefficients():
    """ test the removal of monomials with zero coefficient """
    # pylint: disable=unidiomatic-typecheck
    poly_with_zero = {(3,): 0, (2,3): 0}
    poly_with_zero.update(P1_DICT)
    poly = PolyBinary(poly_with_zero)
    assert poly == POLY1
    assert dict(poly) != dict(POLY1)

    poly_nonzero = poly.remove_zero_coefficients()
    assert poly_nonzero == POLY1
    assert dict(poly_nonzero) == dict(POLY1)
    assert type(poly_nonzero) is PolyBinary

def test_preprocess():
    """ test preprocessing """
    exp_poly = PolyBinary({(): 2})
    exp_variables = {1 : 0, 2 : 0, 3 : 1, 4 : 1}
    preprocessed_poly, preprocessed_variables = POLY1.preprocess()
    assert preprocessed_poly == exp_poly
    assert preprocessed_variables == exp_variables
    preprocessed_poly, preprocessed_variables = POLY1.preprocess(unambitious=True)
    assert preprocessed_poly == exp_poly
    assert preprocessed_variables == exp_variables

    exp_poly = PolyBinary({(): -2})
    exp_variables = {("x", 1) : 1, ("y", 2, 3) : 1, ("y", 1, 2) : 0}
    preprocessed_poly, preprocessed_variables = POLY2.preprocess()
    assert preprocessed_poly == exp_poly
    assert preprocessed_variables == exp_variables
    preprocessed_poly, preprocessed_variables = POLY2.preprocess(unambitious=True)
    assert preprocessed_poly == exp_poly
    assert preprocessed_variables == exp_variables

    poly = PolyBinary({(0,): 10, (1,): 2, (2,): -8, (3,): 10, (4,): -7, (5,): 4, (0, 1): 2, (0, 2): -10, (0, 3): -10,
                       (1, 3): -8, (1, 4): -7, (2, 3): 8, (2, 5): 1, (3, 5): 2})
    exp_variables = {0: 0, 1: 1, 2: 1, 3: 0, 4: 1, 5: 0}
    preprocessed_poly, preprocessed_variables = poly.preprocess()
    assert preprocessed_poly == -20
    assert preprocessed_variables == exp_variables

    exp_variables = {1: 1, 2: 1, 4: 1, 5: 0}
    exp_poly = PolyBinary({(): -20, (0,): 2, (3,): 10, (0, 3): -10})
    preprocessed_poly, preprocessed_variables = poly.preprocess(unambitious=True)
    assert preprocessed_poly == exp_poly
    assert preprocessed_variables == exp_variables

def test_to_ising():
    """ test conversion to Ising polynomials """
    poly_binary = PolyBinary.read_from_string("5 x1 + 6 x2 + 3 x1 x3 - x4 - 3 x2 x4 + 3")
    exp_poly_ising = 0.25 * PolyIsing.read_from_string("32 + 13 x1 + 9 x2 + 3 x3 - 5 x4 + 3 x1 x3 - 3 x2 x4")
    exp_poly_ising_inv = 0.25 * Polynomial.read_from_string("32 - 13 x1 - 9 x2 - 3 x3 + 5 x4 + 3 x1 x3 - 3 x2 x4")
    assert poly_binary.to_ising() == exp_poly_ising
    assert poly_binary.to_ising(inverted=True) == exp_poly_ising_inv

    exp_poly_ising = PolyIsing({(): 8.0, (1,): 1.75, (1, 3): -0.75, (2,): 3.75, (2, 4): 0.75, (3,): -0.75, (4,): 0.25})
    exp_poly_ising_inv = exp_poly_ising.invert()
    assert POLY1.to_ising() == exp_poly_ising
    assert POLY1.to_ising(inverted=True) == exp_poly_ising_inv

    exp_poly_ising = PolyIsing({(): 1.0, (("x", 1),): -0.5, (("y", 1, 2),): 1.5, (("y", 2, 3),): -0.5,
                                (("x", 1), ("y", 2, 3)): -0.5})
    exp_poly_ising_inv = exp_poly_ising.invert()
    assert POLY2.to_ising() == exp_poly_ising
    assert POLY2.to_ising(inverted=True) == exp_poly_ising_inv

def test_unknown_poly_to_binary():
    """ test conversion to binary polynomial """
    assert PolyBinary.from_unknown_poly(POLY1) == POLY1

    poly = PolyBinary.from_unknown_poly(POLY1_NONE)
    assert poly == POLY1_NONE
    assert isinstance(poly, PolyBinary)

    assert PolyBinary.from_unknown_poly(POLY1_ISING) == POLY1
    assert PolyBinary.from_unknown_poly(POLY1_ISING_INV) == POLY1

def test_affine_transform():
    """ test affine transformation """
    # pylint: disable=unidiomatic-typecheck
    exp_poly = Polynomial({(4,) : 33, () : 43, (2, 4) : 27, (2,) : 54, (1, 3) : -27, (1,) : -21, (3,) : -36})
    transformed_poly = POLY1.affine_transform(3, 4)
    assert transformed_poly == exp_poly
    assert type(transformed_poly) is Polynomial

def test_evaluate():
    """ test evaluation """
    # pylint: disable=unidiomatic-typecheck
    var_assignment = {("x", 1): -1, ("y", 2, 3): 1}
    evaluated_poly = POLY2.evaluate(var_assignment)
    exp_poly = PolyBinary({(): 2, (("y", 1, 2),): 3})
    assert evaluated_poly == exp_poly
    assert type(evaluated_poly) is PolyBinary

    var_assignment = {("x", 1) : 0, ("y", 2, 3) : 1, ("y", 1, 2) : 0}
    evaluated_poly = POLY2.evaluate(var_assignment)
    assert evaluated_poly == 0

    with pytest.raises(ValueError, match="Can only assign numbers or polynomials"):
        var_assignment = {("x", 1): PolyBinary({(("x", 1),): 2, (): 3})}
        POLY2.evaluate(var_assignment)

    var_assignment = {("x", 1): Polynomial({(("x", 1),): 2, (): 3})}
    exp_poly = Polynomial({(("x", 1), ("y", 2, 3)): -4, (("y", 1, 2),): 3, (("y", 2, 3),): -6})
    evaluated_poly = POLY2.evaluate(var_assignment)
    assert type(evaluated_poly) is Polynomial
    assert evaluated_poly == exp_poly

def test_replace_variables():
    """ test replacement of variables in polynomials """
    # pylint: disable=unidiomatic-typecheck
    replacement = {("x", 1) : "a", ("y", 2, 3) : "b", ("y", 1, 2) : "c"}
    exp_poly = PolyBinary({("a", "b"): -2, ("c",): 3})
    replaced_poly = POLY2.replace_variables(replacement)
    assert replaced_poly == exp_poly
    assert type(replaced_poly) is PolyBinary

def test_get_rounded():
    """ test rounding of polynomials """
    # pylint: disable=unidiomatic-typecheck
    poly = PolyBinary({(("x", 1), ("y", 2, 3)): -2.3, (("y", 1, 2),): 2.9})
    rounded_poly = poly.get_rounded(0)
    assert rounded_poly == POLY2
    assert type(rounded_poly) is PolyBinary

def test_get_matrix_representation():
    """ test standard matrix representation """
    with pytest.warns(UserWarning, match="Constant offset of 16 is dropped"):
        quadratic = POLY3.get_matrix_representation()
    assert np.array_equal(quadratic, POLY3_QUADRATIC)

def test_get_from_matrix_representation():
    """ test conversion from matrix representation """
    poly = PolyBinary.get_from_matrix_representation(POLY3_QUADRATIC)
    assert poly == POLY3 - 16

    poly = PolyBinary.get_from_matrix_representation(POLY3_LINEAR, POLY3_QUAD_NO_DIAG)
    assert poly == POLY3 - 16

def test_reduce():
    """ test reduction of polynomial """
    qubic = PolyBinary({(1, 2, 3): 1, (2, 3, 4): 2})
    exp_reduced_poly = PolyBinary({(("reduction", "x", 3, "reduction", "x", 1, "x", 2),): 1,
                                   (("reduction", "x", 4, "reduction", "x", 2, "x", 3),): 2})
    exp_penalty_term1 = PolyBinary({(("reduction", "x", 1, "x", 2),): 3,
                                    (("reduction", "x", 1, "x", 2), ("x", 1)): -2,
                                    (("reduction", "x", 1, "x", 2), ("x", 2)): -2,
                                    (("x", 1), ("x", 2)): 1})
    exp_penalty_term2 = PolyBinary({(("reduction", "x", 2, "x", 3),): 3,
                                    (("reduction", "x", 2, "x", 3), ("x", 2)): -2,
                                    (("reduction", "x", 2, "x", 3), ("x", 3)): -2,
                                    (("x", 2), ("x", 3)): 1})
    exp_penalty_term3 = PolyBinary({(("reduction", "x", 3, "reduction", "x", 1, "x", 2),): 3,
                                    (("reduction", "x", 1, "x", 2),
                                     ("reduction", "x", 3, "reduction", "x", 1, "x", 2)): -2,
                                    (("reduction", "x", 1, "x", 2), ("x", 3)): 1,
                                    (("reduction", "x", 3, "reduction", "x", 1, "x", 2), ("x", 3)): -2})
    exp_penalty_term4 = PolyBinary({(("reduction", "x", 4, "reduction", "x", 2, "x", 3),): 3,
                                    (("reduction", "x", 2, "x", 3),
                                     ("reduction", "x", 4, "reduction", "x", 2, "x", 3)): -2,
                                    (("reduction", "x", 2, "x", 3), ("x", 4)): 1,
                                    (("reduction", "x", 4, "reduction", "x", 2, "x", 3), ("x", 4)): -2})
    exp_penalty_terms = {"reduction_x_1_x_2": exp_penalty_term1,
                         "reduction_x_2_x_3": exp_penalty_term2,
                         "reduction_x_3_reduction_x_1_x_2": exp_penalty_term3,
                         "reduction_x_4_reduction_x_2_x_3": exp_penalty_term4}

    with pytest.warns(UserWarning, match="Flat variables are replaced by tuple variables"):
        reduced_poly, penalty_terms = qubic.reduce(reduction_variable_prefix="reduction")
    assert reduced_poly.degree == 1
    assert reduced_poly == exp_reduced_poly
    assert penalty_terms == exp_penalty_terms

    exp_reduced_poly = PolyBinary({(("reduction", "x", 1, "x", 2), ("x", 3)): 1,
                                   (("reduction", "x", 2, "x", 3), ("x", 4)): 2})
    exp_penalty_terms = {"reduction_x_1_x_2": exp_penalty_term1, "reduction_x_2_x_3": exp_penalty_term2}

    with pytest.warns(UserWarning, match="Flat variables are replaced by tuple variables"):
        reduced_poly, penalty_terms = qubic.reduce(max_degree=2)
    assert reduced_poly.degree == 2
    assert reduced_poly == exp_reduced_poly
    assert penalty_terms == exp_penalty_terms

    with pytest.raises(ValueError, match="Unknown option 'sth_else'"):
        with pytest.warns(UserWarning, match="Flat variables are replaced by tuple variables"):
            qubic.reduce(var_pair_choice="sth_else")

def test_reduce_options():
    """ test reduction of polynomial with different options to choose the next variable pair to be replaced """
    poly = PolyBinary({(1, 2, 3, 4): 1, (2, 3, 4, 5): 2, (3, 4): 3})
    exp_poly = PolyBinary({(("reduction", "x", 2, "x", 3), ("x", 1), ("x", 4)): 1,
                           (("reduction", "x", 2, "x", 3), ("x", 4), ("x", 5)): 2,
                           (("x", 3), ("x", 4)): 3})
    exp_penalty_terms = {"reduction_x_2_x_3": PolyBinary({(("reduction", "x", 2, "x", 3),): 3,
                                                          (("reduction", "x", 2, "x", 3), ("x", 2)): -2,
                                                          (("reduction", "x", 2, "x", 3), ("x", 3)): -2,
                                                          (("x", 2), ("x", 3)): 1})}

    with pytest.warns(UserWarning, match="Flat variables are replaced by tuple variables"):
        reduced_poly, penalty_terms = poly.reduce(max_degree=3, var_pair_choice=BETTER)
    assert reduced_poly.degree == 3
    assert reduced_poly == exp_poly
    assert penalty_terms == exp_penalty_terms

    exp_poly = PolyBinary({(("reduction", "x", 3, "x", 4),): 3,
                           (("reduction", "x", 3, "x", 4), ("x", 1), ("x", 2)): 1,
                           (("reduction", "x", 3, "x", 4), ("x", 2), ("x", 5)): 2})
    exp_penalty_terms = {"reduction_x_3_x_4": PolyBinary({(("reduction", "x", 3, "x", 4),): 3,
                                                          (("reduction", "x", 3, "x", 4), ("x", 3)): -2,
                                                          (("reduction", "x", 3, "x", 4), ("x", 4)): -2,
                                                          (("x", 3), ("x", 4)): 1})}

    with pytest.warns(UserWarning, match="Flat variables are replaced by tuple variables"):
        reduced_poly, penalty_terms = poly.reduce(max_degree=3, var_pair_choice=BEST)
    assert reduced_poly.degree == 3
    assert reduced_poly == exp_poly
    assert penalty_terms == exp_penalty_terms

def test_get_reduction_penalty_polynomial():
    """ test creation of penalty polynomial """
    x, y, xy = ("x",), ("y",), ("reduction",)
    reduction_poly = get_reduction_penalty_polynomial(x, y, xy)
    for x_val, y_val, xy_val in product([0, 1], [0, 1], [0, 1]):
        reduction_value = reduction_poly.evaluate({x: x_val, y: y_val, xy: xy_val})
        if x_val * y_val == xy_val:
            assert reduction_value == 0
        else:
            assert reduction_value > 0

def test_reduce_quadratic_to_linear_polynomial():
    """ test reduction to quadratic polynomial """
    exp_poly = PolyBinary({(("reduction", "x", 1, "x", 2),): 2, (("x", 1),): 7, (): 3})
    exp_reduction_poly = get_reduction_penalty_polynomial(("x", 1), ("x", 2), ("reduction", "x", 1, "x", 2))
    exp_reduction_name = "reduction_x_1_x_2"
    reduced_poly, reductions = QUADRATIC_POLY.reduce()
    assert reduced_poly == exp_poly
    assert reductions == {exp_reduction_name: exp_reduction_poly}

    flat_poly = PolyBinary({(2, 1): 2, (1,): 7, (): 3})
    with pytest.warns(UserWarning, match="Flat variables are replaced by tuple variables"):
        reduced_poly, reductions = flat_poly.reduce()
    assert reduced_poly == exp_poly
    assert reductions == {exp_reduction_name: exp_reduction_poly}

    reduced_poly, reductions = LINEAR_POLY.reduce()
    assert reduced_poly == LINEAR_POLY
    assert not reductions

def test_get_slack_polynomial():
    """ test construction of slack polynomial """
    slack_poly = get_binary_representation_polynomial(0)
    exp_poly = PolyBinary()
    assert slack_poly == exp_poly

    slack_poly = get_binary_representation_polynomial(1)
    exp_poly = PolyBinary({(("x", 0),): 1})
    assert slack_poly == exp_poly

    slack_poly = get_binary_representation_polynomial(2)
    exp_poly = PolyBinary({(("x", 0),): 1, (("x", 1),): 1})
    assert slack_poly == exp_poly

    slack_poly = get_binary_representation_polynomial(3)
    exp_poly = PolyBinary({(("x", 0),): 1, (("x", 1),): 2})
    assert slack_poly == exp_poly

    slack_poly = get_binary_representation_polynomial(4)
    exp_poly = PolyBinary({(("x", 0),): 1, (("x", 1),): 2, (("x", 2),): 1})
    assert slack_poly == exp_poly

    slack_poly = get_binary_representation_polynomial(5)
    exp_poly = PolyBinary({(("x", 0),): 1, (("x", 1),): 2, (("x", 2),): 2})
    assert slack_poly == exp_poly

    slack_poly = get_binary_representation_polynomial(7, "slack")
    exp_poly = PolyBinary({(("slack", 0),): 1, (("slack", 1),): 2, (("slack", 2),): 4})
    assert slack_poly == exp_poly

    with pytest.raises(ValueError, match="Upper bound needs to be larger than or equal 0"):
        get_binary_representation_polynomial(-7)
