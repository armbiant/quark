# Copyright 2022 DLR-SC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

""" module for testing the Embedding """

import pytest

from quark import Embedding, HardwareAdjacency


NAME = "test_embedding"
VAR_NODES_MAP = {0: [4, 0, 24, 48],
                 1: [5, 1, 25, 49],
                 2: [6, 2, 26, 50],
                 3: [7, 3, 27, 51],
                 4: [28, 36, 32, 56],
                 5: [29, 37, 33, 57],
                 6: [30, 38, 34, 58],
                 7: [31, 39, 35, 59],
                 8: [68, 64, 60, 52],
                 9: [69, 65, 61, 53],
                 10: [70, 66, 62, 54],
                 11: [71, 67, 63, 55]}


def test_init():
    """ test initialization of embedding """
    embedding = Embedding(var_nodes_map=VAR_NODES_MAP, name=NAME, index=0)
    assert set(embedding.nodes) < set(range(104))
    assert embedding.max_map_size == 4
    assert embedding.edges == []

def test_consistency():
    """ test correctly thrown errors """
    with pytest.raises(ValueError, match="Either provide mapping from variables to nodes or to edges"):
        Embedding(name="nothing")
    with pytest.raises(ValueError, match="The var_edges_map cannot contain different variables than var_nodes_map"):
        Embedding(var_nodes_map={0: [0], 1: [1]}, var_edges_map={0: [(0, 2)]}, name="wrong_nodes")

    embedding = Embedding(var_nodes_map={0: [0], 1: [1]}, name="no_maps")
    with pytest.raises(ValueError, match="Cannot judge validity without mapping from couplings to edges"):
        embedding.are_all_couplings_mapped([(0, 1)])
    with pytest.raises(ValueError, match="Cannot check connectivity without mapping from variables to edges"):
        embedding.are_all_subgraphs_connected()

def test_get_from_hwa():
    """ test construction of embedding with a given HWA """
    edges = [(0, 1), (1, 2), (0, 3), (1, 4), (2, 5), (3, 4), (4, 5), (6, 3), (7, 4), (8, 5), (6, 7), (7, 8)]
    hwa = HardwareAdjacency(edges, "test_hwa")  # 3x3 grid example
    var_nodes_map = {"a": [0, 1], "b": [3, 4], "c": [2, 5]}
    embedding = Embedding.get_from_hwa(var_nodes_map, hwa)
    couplings = [("a", "b"), ("a", "c"), ("c", "b")]

    exp_embedding = Embedding(var_nodes_map={'a': [0, 1], 'b': [3, 4], 'c': [2, 5]},
                              var_edges_map={'a': [(0, 1)], 'b': [(3, 4)], 'c': [(2, 5)]},
                              coupling_edges_map={('a', 'b'): [(0, 3), (1, 4)],
                                                  ('a', 'c'): [(1, 2)],
                                                  ('b', 'c'): [(4, 5)]},
                              name='test_hwa')
    not_exp_embedding = Embedding(var_nodes_map={'a': [0, 1], 'b': [3, 4], 'c': [2, 5]},
                                  var_edges_map={'a': [(0, 1)], 'b': [(3, 4)], 'c': [(2, 5)]},
                                  coupling_edges_map={('a', 'b'): [(0, 3)],
                                                      ('a', 'c'): [(1, 2)],
                                                      ('b', 'c'): [(4, 5)]},
                                  name='test_hwa')
    assert embedding == exp_embedding
    assert embedding != not_exp_embedding
    assert embedding != {'a': [0, 1], 'b': [3, 4], 'c': [2, 5]}

    assert embedding.is_valid(couplings)
    assert set(embedding.edges) > {(0, 1), (0, 3), (1, 2)}
    assert embedding.name == hwa.name

    embedding_only_edges = Embedding.get_from_hwa(var_nodes_map, edges, name=hwa.name)
    assert embedding == embedding_only_edges

def test_checks():
    """ test the different validity checks """
    embedding = Embedding(var_nodes_map={0: [0, 4], 1: [1, 6], 2: [2, 6]})
    assert not embedding.are_all_node_sets_disjoint()
    assert not embedding.is_valid()

    embedding = Embedding(var_nodes_map={0: [0, 4], 1: [1, 5], 2: [2, 6]},
                          var_edges_map={0: [(0, 4)], 1: [], 2: [(2, 6)]})
    assert not embedding.are_all_subgraphs_connected()
    assert not embedding.is_valid()

    embedding = Embedding(var_nodes_map={0: [0, 4], 1: [1], 2: [2, 6]},
                          var_edges_map={0: [(0, 4)], 1: [], 2: [(2, 6)]})
    assert embedding.are_all_subgraphs_connected()
    assert embedding.is_valid()

    embedding = Embedding(var_nodes_map={0: [0, 4], 1: [], 2: [2, 6]},
                          var_edges_map={0: [(0, 4)], 1: [], 2: [(2, 6)]})
    assert not embedding.are_all_subgraphs_connected()
    assert not embedding.is_valid()

    embedding = Embedding(coupling_edges_map={(0, 1): [(0, 5)]},
                          var_edges_map={0: [(0, 4)], 1: [(1, 5)], 2: [(2, 6)]})
    assert not embedding.are_all_couplings_mapped([(0, 2)])
    assert embedding.is_valid()
    assert not embedding.is_valid([(0, 2)])

    embedding = Embedding(coupling_edges_map={(0, 1): [(0, 5)]},
                          var_edges_map={0: [(0, 4)], 1: [], 2: [(2, 6)]})
    assert not embedding.are_all_edges_valid()
    assert not embedding.is_valid()
